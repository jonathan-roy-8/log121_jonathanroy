import java.util.ArrayList;
import java.util.Iterator;


/**
 * Classe qui émerge de la fusion de deux restaurants: Chez Berte et Fonta
 * Exemple basé sur Freeman, E. and Robson, E. and Bates, B. and Sierra, K. 
 * Head first design patterns. O'Reilly Media, Incorporated. 2004.
 */
public class ChezBerteFonta {

	ArrayList<MenuToIterate> menus; // liste de menu
	
	/**
	 * Constructor
	 */
	public ChezBerteFonta(){
		menus = new ArrayList<MenuToIterate>();
		MenuBerte menuBerte = new MenuBerte();
		MenuFonta menuFonta = new MenuFonta();
		menus.add(menuBerte);
		menus.add(menuFonta);
	}
	
	/**
	 * This function prints the items of all menus
	 */
	public void afficherMenus(){
		Iterator<MenuItem> it;
		for(MenuToIterate menu : menus){
			it = menu.createIterator();
			while(it.hasNext()){
				MenuItem item = (MenuItem ) it.next();
				System.out.println(item.getString());
			}
		}
	}
	
	/**
	 * For testing
	 * @param args
	 */
	public static void main(String[] args){
		ChezBerteFonta resto = new ChezBerteFonta();
		resto.afficherMenus();
	}
}
