import java.util.ArrayList;
import java.util.Iterator;

/**
 * Menu of Chez Berte restaurant
 *
 */
public class MenuBerte implements MenuToIterate{
	private ArrayList<MenuItem> menuItems ; // list of menu items
	
	/**
	 * Constructor
	 */
	public MenuBerte() {
		menuItems = new ArrayList<MenuItem>() ;
		addItem("Poutine au tofu", "Patates douces, sauce hollandaise et tofu",true, 6.99) ;
		addItem("tourtière de Berte",
		"Pâté de la viande de caribou",false, 10.49);
		addItem("Caviar à la citronnelle",
				"oeufs de homard avec citron et échalotes",false, 25.00) ;
	}
	
	public ArrayList<MenuItem> getMenuItems(){return menuItems;}
	
	/**
	 * Add an item
	 * @param name item's name
	 * @param description description
	 * @param vegetarian if the item is vegetarian
	 * @param price item's price
	 */
	public void addItem(String name, String description, boolean vegetarian, double price){
		MenuItem menuItem = new MenuItem(name, description,
		vegetarian, price) ;
		menuItems.add(menuItem) ;
	}

	/**
	 * @return an Iterator no the MenuItem list
	 */
	public Iterator<MenuItem> createIterator() {
	   // TODO Auto-generated method stub
	    return menuItems.iterator();
    }
}
