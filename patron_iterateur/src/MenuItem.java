
/**
 * A menu item
 *
 */
public class MenuItem{
	
	   private String name;
	   private String description;
	   private boolean vegetarian;
	   private double price;
	  
	   /**
	    * Constructor
	    * @param name
	    * @param description
	    * @param vegetarian
	    * @param price
	    */
	   public MenuItem(String name, String description, 
	                     boolean vegetarian, double price){
		   this.name = name;
 		   this.description = description;
 		   this.vegetarian = vegetarian;
 		   this.price = price;
	   }
	   
	   /**
	    * 
	    * @return item's name
	    */
	   public String getName(){
		   return name;
	   }
	   
	   /**
	    * 
	    * @return item's description
	    */
	   public String getDescription(){
		   return description;
	   }
	   
	   /**
	    * 
	    * @return is vegetarian
	    */
	   public boolean isVegetarian(){
		   return vegetarian;
	   }
	   
	   /**
	    * 
	    * @return item's price
	    */
	   public double getPrice(){
		   return price;
	   }
	   
	   /**
	    * 
	    * @return a string that represents the item
	    */
	   public String getString(){
		   String svege;
		   if(vegetarian)
			   svege = "vege";
		   else
			   svege = "non vege";
		   String s = name+"("+description+"-"+svege+")"+": "+price;
		   return s;
	   }
	}