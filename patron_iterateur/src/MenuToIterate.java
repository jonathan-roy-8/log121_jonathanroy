import java.util.Iterator;

/**
 * Menu interface
 *
 */
public interface MenuToIterate {

	Iterator<MenuItem> createIterator();
}
