import java.util.ArrayList;
 

/**
 * Classe qui émerge de la fusion de deux restaurants: Chez Berte et Fonta
 * Exemple basé sur Freeman, E. and Robson, E. and Bates, B. and Sierra, K. 
 * Head first design patterns. O'Reilly Media, Incorporated. 2004.
 */
public class ChezBerteFontaV0 {

	MenuBerte menuBerte = new MenuBerte(); 
	MenuFonta menuFonta = new MenuFonta();
	
	/**
	 * Constructor
	 */
	public ChezBerteFontaV0(){
	}
	
	/**
	 * This function prints the items of all menus
	 */
	public void printMenus(){
		ArrayList<MenuItem> menuItemsBerte = menuBerte.getMenuItems();
		for(MenuItem menuItem : menuItemsBerte){
		   System.out.println(menuItem.getName() + " ");
		   System.out.println(menuItem.getPrice() + " ");
		   System.out.println(menuItem.getDescription() + " ");
		}
		MenuItem[] menuItemsFonta = menuFonta.getMenuItems(); 
		for(int i=0; i<menuItemsFonta.length; i++){
		   MenuItem menuItem = menuItemsFonta[i];
		   System.out.println(menuItem.getName() + " ");
		   System.out.println(menuItem.getPrice() + " ");
		   System.out.println(menuItem.getDescription() + " ");
		}
	}
	
	/**
	 * For testing
	 * @param args
	 */
	public static void main(String[] args){
		ChezBerteFonta resto = new ChezBerteFonta();
		resto.afficherMenus();
	}
}
