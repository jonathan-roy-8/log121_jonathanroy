import java.util.Iterator;


/**
 * Menu of Fonta restaurant
 *
 */
public class MenuFonta implements MenuToIterate{

   public static final int MAX_ITEMS = 6; // Maximal number of items
   private int numberOfItems = 0; // number of items
   private MenuItem[] menuItems; // the items list
 
   /**
    * Constructor of MenuFonta
    */
   public MenuFonta() {
      menuItems = new MenuItem[MAX_ITEMS];
      addItem("Fondu au fromage","fromage fondu et bouts de pain",true, 15.00);
      addItem("Fondu chinoise"," tranches de boeuf et sauces mayonnaises",true, 15.00);
      addItem("Fondu au chocolat","chocolat suisse et fruits frais",true, 10.00);
   }	
   
   public MenuItem[] getMenuItems(){return menuItems;}
   
   /**
	 * Add an item
	 * @param name item's name
	 * @param description description
	 * @param vegetarian if the item is vegetarian
	 * @param price item's price
	 */
   public void addItem(String name, String description, 
		   boolean vegetarian, double price) throws ArrayIndexOutOfBoundsException {
      MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
      menuItems[numberOfItems] = menuItem;
      numberOfItems = numberOfItems + 1; 
   } 


   @Override

   public Iterator<MenuItem> createIterator() {
	// TODO Auto-generated method stub
	   return new ArrayIterator<MenuItem>(menuItems);
   }
}
