
package matrixtest;

import matrix.MatrixV1;
import junit.framework.*;

public class TestMatrixV1 extends TestCase{

	public void testIdentity(){
		//public static MatrixV1 identity(int inbColumns)
		MatrixV1 id = MatrixV1.identity(3);
		double[][] dataref = {{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}};
		MatrixV1 ref = new MatrixV1(dataref);
		assertTrue(id.eq(ref));
	}
	
	public void testSwap(){
		double[][] dataA = {{1.0,2.0,3.0},{4.0,5.0,6.0},{7.0,8.0,9.0}};
		MatrixV1 A = new MatrixV1(dataA);
		A.swap(0,2);
		double[][] dataref = {{7.0,8.0,9.0},{4.0,5.0,6.0},{1.0,2.0,3.0}};
		MatrixV1 ref = new MatrixV1(dataref);
		assertTrue(A.eq(ref));
	}
	
	public void testTranspose(){
		double[][] dataA = {{1.0,2.0,3.0},{4.0,5.0,6.0},{7.0,8.0,9.0}};
		MatrixV1 A = new MatrixV1(dataA);
		MatrixV1 R = A.transpose();
		double[][] dataref = {{1.0, 4.0, 7.0},{2.0, 5.0, 8.0},{3.0, 6.0, 9.0}};
		MatrixV1 ref = new MatrixV1(dataref);
		assertTrue(R.eq(ref));	
	}
	
	public void testPlus(){
		double[][] dataA = {{1.0,2.0,3.0},{4.0,5.0,6.0},{7.0,8.0,9.0}};
		MatrixV1 A = new MatrixV1(dataA);
		double[][] dataB = {{2.0,4.0,6.0},{8.0,10.0,12.0},{14.0,16.0,18.0}};
		MatrixV1 B = new MatrixV1(dataB);
		MatrixV1 R = A.plus(B);
		double[][] dataref = {{3.0,6.0,9.0},{12.0,15.0,18.0},{21.0,24.0,27.0}};
		MatrixV1 ref = new MatrixV1(dataref);
		assertTrue(R.eq(ref));	
	}
	
	public void testMinus(){
		double[][] dataA = {{1.0,2.0,3.0},{4.0,5.0,6.0},{7.0,8.0,9.0}};
		MatrixV1 A = new MatrixV1(dataA);
		double[][] dataB = {{2.0,4.0,6.0},{8.0,10.0,12.0},{14.0,16.0,18.0}};
		MatrixV1 B = new MatrixV1(dataB);
		MatrixV1 R = A.minus(B);
		double[][] dataref = {{-1.0,-2.0,-3.0},{-4.0,-5.0,-6.0},{-7.0,-8.0,-9.0}};
		MatrixV1 ref = new MatrixV1(dataref);
		assertTrue(R.eq(ref));	
	}
	
	public void testEq(){
		double[][] dataA = {{1.0,2.0,3.0},{4.0,5.0,6.0},{7.0,8.0,9.0}};
		MatrixV1 A = new MatrixV1(dataA);
		double[][] dataB = {{1.0,2.0,3.0},{4.0,5.0,6.0},{7.0,8.0,9.0}};
		MatrixV1 B = new MatrixV1(dataB);
		assertTrue(A.eq(B));
		
		double[][] dataC = {{1.0,2.0,3.0},{4.0,5.0,6.0},{-7.0,8.0,9.0}};
		MatrixV1 C = new MatrixV1(dataC);
		assertFalse(A.eq(C));
	}
	
	public void testTimes(){
		double[][] dataA = {{1.0, 2.0, 3.0},{4.0, 5.0, 6.0},{7.0, 8.0, 9.0}};
		MatrixV1 A = new MatrixV1(dataA);
		double[][] dataB = {{3.0, -1.0},{-1.0, 2.0},{1.0, -2.0}};
		MatrixV1 B = new MatrixV1(dataB);
		MatrixV1 R = A.times(B);
		double[][] dataref = {{4.0, -3.0},{13.0,-6.0},{22.0,-9.0}};
		MatrixV1 ref = new MatrixV1(dataref);
		R.show();
		ref.show();
		assertTrue(R.eq(ref));	
	}
	
	public void testSolve(){
		
	}
}
