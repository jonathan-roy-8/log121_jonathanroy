
package badmatrix;
import java.util.ArrayList;




/** Mauvaise conception d'une classe matrice, car
* les attributs "public" peuvent être utilisés à l'extérieur de la classe. 
* Par conséquent :
*    - Une modification des attributs (type ou nom) pourra générer des erreurs 
*      dans les classes qui les utilisent directement (BadMatrixUser).
*      La classe pourra donc difficilement évoluer
*    - Une modification de la valeur d'un attribut à l'extérieur de la classe
*      (exemple: objet.matrix = null) pourrait générer des erreurs d'exécution des méthodes 
*
*  Cette classe manque aussi de complétude: elle ne fournit pas les opérations matricielles de base
*  
*   @author Patrice Boucher
*/
public class BadMatrix {

	public ArrayList<ArrayList<Double> > matrix;
	
	/**
	 * Constructeur
	 * @param dimRow
	 * @param dimColumn
	 */
	public BadMatrix(int dimRow, int dimColumn){
		int i,j;
		matrix = new ArrayList<ArrayList<Double> >();
		ArrayList<Double> row;
		for(i=0; i< dimRow ; ++i){
			row = new ArrayList<Double>();
			for(j=0 ; j< dimColumn ; ++j)
				row.add(0.0);
			matrix.add(row);
		}
	}
}
