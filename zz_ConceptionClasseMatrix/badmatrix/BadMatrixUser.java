

package badmatrix;
 
/** Mauvaise conception d'un utilisateur d'une matrice "BadMatrix", 
    qui utilise directement les attributs d'une matrice 
    pour effectuer des opérations qui devraient être fournit 
    par la classe BadMatrix
    Règle générale: 
       une classe qui utilise des classes mal conçues devient souvent, elle-même, problématique. 
 */
public class BadMatrixUser {

	
	/**
	 * Calcul l'inverse d'une matrice A = [1 2 ; 3 4]
	 * Cette méthode compense le manque de complétude de BadMatrix,
	 *   en jouant directement dans ses attributs pour arriver au résultat.
	 *   Toute modification de l'implémentation de BadMatrix (exemple: changement de structure de données)
	 *   pourrait rendre ce code dysfonctionnel. 
	 */
	public void doComplexOperation(){
		BadMatrix matA = new BadMatrix(2,2);
		matA.matrix.get(0).set(0,1.0);
		matA.matrix.get(0).set(1,2.0); 
		matA.matrix.get(1).set(0,3.0); 
		matA.matrix.get(1).set(1,4.0); 
		// Besoin de calculer l'inverse de A.
		//  Trouver le déterminant
		double det = matA.matrix.get(0).get(0)*matA.matrix.get(1).get(1)-
				       matA.matrix.get(0).get(1)*matA.matrix.get(1).get(0);
		//  calcul la matrice inverse
		BadMatrix matAT = new BadMatrix(2,2);
		matAT.matrix.get(0).set(0,matA.matrix.get(1).get(1)/det);
		matAT.matrix.get(0).set(1,matA.matrix.get(0).get(1)*-1/det); 
		matAT.matrix.get(1).set(0,matA.matrix.get(1).get(0)*-1/det); 
		matAT.matrix.get(1).set(1,matA.matrix.get(0).get(0)*-1/det); 
		System.out.println("A^{-1} = [ "+matAT.matrix.get(0).get(0)+" "+matAT.matrix.get(0).get(1));
		System.out.println("           "+matAT.matrix.get(1).get(0)+" "+matAT.matrix.get(1).get(1)+" ]");
	}
}
