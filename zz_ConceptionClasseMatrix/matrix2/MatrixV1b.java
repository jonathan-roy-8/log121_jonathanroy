package matrix2;

import java.util.ArrayList;

import matrix.MatrixV1;

/**
 * Représente une matrice dont les éléments sont stockés dans des conteneurs de type ArrayList
 * 
 * This entire class is based on the code on:
 *    http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
 * Some modifications of implementations are :
 * -  Usage of ArrayList to contain elements 
 * -  Add a method: operationEE that allows any element to element operations
 * -  Add javadoc comments
 * 
 * @author Patrice Boucher
 */
public class MatrixV1b extends Matrix{

	private ArrayList<ArrayList<Double> > matrix;

	/**
	 * Constructor
	 * @param nbRows number of rows
	 * @param nbColumns number of columns
	 */
	public MatrixV1b(int nbRows, int nbColumns) {
		super(nbRows, nbColumns);
		int i,j;
		matrix = new ArrayList<ArrayList<Double> >();
		ArrayList<Double> row;
		for(i=0; i< nbRows ; ++i){
			row = new ArrayList<Double>();
			for(j=0 ; j< nbColumns ; ++j)
				row.add(0.0);
			matrix.add(row);
		}
	}
	
	/**
	 * Create matrix based on 2d array
	 * @param data
	 */
    public MatrixV1b(double[][] data) {
    	super(data.length, data[0].length);
        matrix = new ArrayList<ArrayList<Double> >();
		ArrayList<Double> row;
		int i,j;
		for(i=0; i< nbRows ; ++i){
			row = new ArrayList<Double>();
			for(j=0 ; j< nbColumns ; ++j)
				row.add(data[i][j]);
			matrix.add(row);
		}
    }
    
    /**
     * Copy constructor
     * @param A the matrix to copy
     */
    private MatrixV1b(MatrixV1 A) { 
    	super(A.getSizeRow(),A.getSizeColumn());
		int i,j;
		matrix = new ArrayList<ArrayList<Double> >();
		ArrayList<Double> row;
		for(i=0; i< nbRows ; ++i){
			row = new ArrayList<Double>();
			for(j=0 ; j< nbColumns ; ++j)
				row.add(A.get(i,j));
			matrix.add(row);
		}
    }

	@Override
	public double get(int irow, int icol) {
		return matrix.get(irow).get(icol);
	}

	@Override
	public void set(int irow, int icol, double value) {
		matrix.get(irow).set( icol, value);
	}

    /**
     * Return a row
     * @param z index of the row
     * @return row 
     */
	protected ArrayList<Double> getRow(int z) {
		// TODO Auto-generated method stub
		return matrix.get(z);
	}
    
	@Override
	public void swap(int y, int z) {
		MatrixV1b tmp= (MatrixV1b) this.clone();
    	matrix = new ArrayList<ArrayList<Double> >();
    	int i;
    	ArrayList<Double> row;
    	for(i=0; i< nbRows ; ++i){
    		if(i==y)
     		  row = tmp.getRow(z);
    		else if(i==z)
    		  row = tmp.getRow(y);
    		else
    		  row = tmp.getRow(i);
			matrix.add(row);
		}
	}
 
	@Override
	public MatrixV1b getEmptyMatrix(int nbRows, int nbColumns) {		
		return new MatrixV1b(nbRows, nbColumns);
	}
	
	@Override
	public MatrixV1b createMatrix(double[][] data) {		
		return new MatrixV1b(data);
	}
 
}
