package matrix2;


import operators.Add;
import operators.Operator;
import operators.Subtract;

/**
 * Classe abstraite qui regroupe les caractéristiques 
 * communes à différentes implémentations de classes matricielles.
 * 
 * This entire class is based on the code on:
 *    http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
 * Some modifications of implementations are :
 * -  Usage of ArrayList to contain elements 
 * -  Add a method: operationEE that allows any element to element operations
 * -  Add javadoc comments
 * 
 * @author Patrice Boucher
 */
public abstract class Matrix {
	protected int nbRows;
	protected int nbColumns;
		
		/**
		 * Constructeur
		 * @param nbRows
		 * @param nbColumns
		 */
		public Matrix(int nbRows, int nbColumns){
			this.nbRows = nbRows;
			this.nbColumns = nbColumns;
		}
  
		
	    /**
	     * Create and return a random M-by-N matrix with values between 0 and 1
	     * @param nbRows index of line
	     * @param nbColumns index of column
	     * @return a random matrix
	     */
	    public Matrix random(int nbRows, int nbColumns) {
	    	Matrix A = this.getEmptyMatrix(nbRows, nbColumns);
	        for (int i = 0; i < nbRows; i++)
	            for (int j = 0; j < nbColumns; j++)
	                A.set(i,j, Math.random());
	        return A;
	    }
		
		/**
		 * Accessor
		 * @precondition 0 <= irow < this.getSizeRow();
		 * @precondition 0 <= icol < this.getSizeColumn();  
		 * @param irow index of line
		 * @param icol index of column
		 * @return selement
		 */
		public abstract double get(int irow, int icol);
		
		/**
		 * Set an matrix's element
		 * @param irow indice de la ligne
		 * @param icol indice de la colonne
		 * @param value valeur de l'élément
		 */
		public abstract void set(int irow, int icol, double value);
		
		/**
		 * @return number of lines
		 */
		public int getSizeRow(){
			return nbRows;
		}
		
		/**
		 * 
		 * @return number of columns
		 */
		public int getSizeColumn(){
			return nbColumns;
		}
		

		/**
		 * Return a matrix obtained from applying an operator between the elements of this matrix
		 * with those of a second matrix
		 * @param matrix2 second matrix
		 * @param op operator
		 * @return matrix
		 * @throws Exception
		 */
		public Matrix operateEE(Matrix matrix2, Operator op) throws Exception{
			if (this.nbRows != matrix2.nbRows || this.nbColumns != matrix2.nbColumns) 
				throw new RuntimeException("Illegal matrix dimensions.");
			Matrix matrix = getEmptyMatrix(nbRows,nbColumns); //new MatrixV1(nbRows,nbColumns);
			int i,j;
			for(i=0 ; i < nbRows ; ++i){
				for(j=0 ; j < nbColumns ; ++j){
					matrix.set(i, j, op.calculate(this.get(i, j), matrix2.get(i, j)));
				}
			}
			return matrix;
		}

	 
		/**
		 * Create and return the N-by-N identity matrix
		 * @param inbColumns
		 * @return matrix
		 */
	    public Matrix identity(int inbColumns) {
	        Matrix I = getEmptyMatrix(inbColumns, inbColumns);
	        for (int i = 0; i < inbColumns; i++)
	            I.set(i,i, 1);
	        return I;
	    }
	    
	    /**
	     * Swap rows y and z
	     * @param y row 1
	     * @param z row 2
	     */
	    public abstract void swap(int y, int z);

	    /**
	     * Create and return the transpose of the invoking matrix
	     * @return transposed matrix
	     */
	    public Matrix transpose() {
	        Matrix A = getEmptyMatrix(nbColumns, nbRows);
	        for (int i = 0; i < nbRows; i++)
	            for (int j = 0; j < nbColumns; j++)
	                A.set(j,i, this.get(i,j));
	        return A;
	    }
	    

	    /**
	     * Add this matrix to a second matrix
	     * @param B second matrix
	     * @return C = this + B
	     */
	    public Matrix plus(Matrix B) {
	        Matrix C=null;
			try {
				C = this.operateEE(B, new Add());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return C;
	    }


	    /**
	     * Subtract this matrix to a second matrix
	     * @param B second matrix
	     * @return C = this - B
	     */
	    public Matrix minus(Matrix B) {
	    	Matrix A = this;
	        if (B.nbRows != A.nbRows || B.nbColumns != A.nbColumns) throw new RuntimeException("Illegal matrix dimensions.");
	        Matrix C = null;
			try {
				C = this.operateEE(B, new Subtract());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return C;
	    }

	    /**
	     * Does A = B exactly?
	     * @param B
	     * @return A = B exactly?
	     */
	    public boolean eq(Matrix B) {
	    	Matrix A = this;
	        if (B.nbRows != A.nbRows || B.nbColumns != A.nbColumns) 
	        	throw new RuntimeException("Illegal matrix dimensions.");
	        for (int i = 0; i < nbRows; i++)
	            for (int j = 0; j < nbColumns; j++)
	                if (A.get(i,j) != B.get(i,j)) return false;
	        return true;
	    }
		
	    /**
	     * Multiply this matrix with a second matrix
	     * @param B second matrix
	     * @return  C = this * B
	     */
	    public Matrix times(Matrix B) {
	    	Matrix A = this;
	        if (A.nbColumns != B.nbRows) throw new RuntimeException("Illegal matrix dimensions.");
	        Matrix C = getEmptyMatrix(A.nbRows, B.nbColumns);
	        for (int i = 0; i < C.nbRows; i++)
	            for (int j = 0; j < C.nbColumns; j++)
	                for (int k = 0; k < A.nbColumns; k++)
	                    C.set(i,j, C.get(i, j)+(A.get(i,k) * B.get(k,j)));
	        return C;
	    }


	    /**
	     * Solve a linear system Y= A*x, with A=this matrix
	     * @param Y
	     * @return x = this^-1 B, assuming A is square and has full rank
	     */
	    public Matrix solve(Matrix B) {
	        if (nbRows != nbColumns || B.nbRows != nbColumns || B.nbColumns != 1)
	            throw new RuntimeException("Illegal matrix dimensions.");

	        // create copies of the data
	        Matrix A = this.clone(); 
	        Matrix b = B.clone();

	        // Gaussian elimination with partial pivoting
	        for (int i = 0; i < nbColumns; i++) {

	            // find pivot row and swap
	            int max = i;
	            for (int j = i + 1; j < nbColumns; j++)
	                if (Math.abs(A.get(j,i)) > Math.abs(A.get(max,i)))
	                    max = j;
	            A.swap(i, max);
	            b.swap(i, max);

	            // singular
	            if (A.get(i,i) == 0.0) throw new RuntimeException("Matrix is singular.");

	            // pivot within b
	            for (int j = i + 1; j < nbColumns; j++)
	                b.set(j,0,b.get(j, 0) - b.get(i,0) * A.get(j,i) / A.get(i,i));

	            // pivot within A
	            for (int j = i + 1; j < nbColumns; j++) {
	                double m = A.get(j,i) / A.get(i,i);
	                for (int k = i+1; k < nbColumns; k++) {
	                    A.set(j,k, A.get(j, k) - A.get(i,k) * m);
	                }
	                A.set(j,i, 0.0);
	            }
	        }
	     // back substitution
	        Matrix x = getEmptyMatrix(nbColumns, 1);
	        for (int j = nbColumns - 1; j >= 0; j--) {
	            double t = 0.0;
	            for (int k = j + 1; k < nbColumns; k++)
	                t += A.get(j,k) * x.get(k,0);
	            x.set(j,0, (b.get(j,0) - t) / A.get(j,j));
	        }
	        return x;
	    }
	    
	    
	    /**
	     * Print matrix to standard output
	     */
	    public void show() {
	        for (int i = 0; i < nbRows; i++) {
	            for (int j = 0; j < nbColumns; j++) 
	                System.out.printf("%9.4f ", get(i,j));
	            System.out.println();
	        }
	    }
	    
		public Matrix clone() {
			Matrix mat2 = getEmptyMatrix(nbRows, nbColumns);
			int i,j;
			for(i=0 ; i < nbRows ; ++i)
			{
				for(j=0; j < nbColumns ; ++j)
					mat2.set(i, j, this.get(i, j));
				
			}
			return mat2;
		}
		
		/**
		 * Create an empty matrix with a specified dimension
		 * @param nbRows number of lines
		 * @param nbColumns number of columns
		 * @return
		 */
	    public abstract Matrix getEmptyMatrix(int nbRows, int nbColumns);
	    
	    /**
	     * Create a matrix from a table of data
	     * @param data
	     * @return matrix
	     */
	    public abstract Matrix createMatrix(double[][] data);
}
