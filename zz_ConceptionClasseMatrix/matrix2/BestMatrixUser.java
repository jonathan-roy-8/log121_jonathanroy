package matrix2;

 

public class BestMatrixUser {

	public void solveALinearSystem(Matrix A){
		   System.out.println("v = [1 2 3]' ");
	       double [][] dv = {{1},{2},{3}};
	       Matrix  v = A.createMatrix(dv);
	       System.out.println("x = A.solve(v)");
	       Matrix  x = A.solve(v);
	       x.show();
	       System.out.println();  
	}
	
	public void doManyOperations(Matrix A)
	{  
	       System.out.println("A");
	       A.show();        
	       System.out.println();
	 
	       A.swap(1, 2);
	       System.out.println("Swap 1 <-> 2");
	       A.show(); 
	       System.out.println();

	       Matrix B = A.transpose();
	       System.out.println("B = A^t");
	       B.show(); 
	       System.out.println();

	       Matrix C = A.identity(3);
	       System.out.println("C = Identity");
	       C.show(); 
	       System.out.println();

	       System.out.println("A+B");
	       A.plus(B).show();
	       System.out.println();

	       System.out.println("B*A");
	       B.times(A).show();
	       System.out.println();

	       // shouldn't be equal since AB != BA in general  
	       System.out.println("B==A");
	       System.out.println(A.times(B).eq(B.times(A)));
	       System.out.println();

	       System.out.println("v =  ");
	       double [][] dv = {{1},{2},{3}};
	       Matrix  v = A.createMatrix(dv);
	       v.show();
	       System.out.println();

	       System.out.println("x = A.solve(v)");
	       Matrix  x = A.solve(v);
	       x.show();
	       System.out.println();

	       System.out.println("Ax");
	       A.times(x).show();
       }
}
