package matrix2;

/**
 * Représente une matrice dont les éléments sont stockés dans un tableau de "double"  
 * 
 * This entire class is based on the code on:
 *    http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
 * Some modifications of implementations are :
 * -  Usage of ArrayList to contain elements 
 * -  Add a method: operationEE that allows any element to element operations
 * -  Add javadoc comments
 * 
 * @author Patrice Boucher
 */
public class MatrixV2b extends Matrix{
	
	private double[][] data;   // M-by-N array
	
	/**
	 * Constructor
	 * @param nbRows number of rows
	 * @param nbColumns number of columns
	 */
	public MatrixV2b(int nbRows, int nbColumns) {
		super(nbRows, nbColumns);
		data = new double[nbRows][nbColumns];
	}
	
	/**
	 * Create matrix based on 2d array
	 * @param data
	 */
    public MatrixV2b(double[][] data) {
    	super(data.length, data[0].length);
        this.data = new double[nbRows][nbColumns];
        for (int i = 0; i < nbRows; i++)
            for (int j = 0; j < nbColumns; j++)
                    this.data[i][j] = data[i][j];
    }
    
    /**
     * Copy constructor
     * @param A the matrix to copy
     */
    public MatrixV2b(MatrixV2b A) { this(A.data); }

	@Override
	public double get(int irow, int icol) {
		return data[irow][icol] ;
	}

	@Override
	public void set(int irow, int icol, double value) {
		data[irow][icol] = value;
	}

	@Override
	public void swap(int y, int z) {
		 double[] temp = data[y];
	        data[y] = data[z];
	        data[z] = temp;
	}

	@Override
	public MatrixV2b getEmptyMatrix(int nbRows, int nbColumns) {
		// TODO Auto-generated method stub
		return new MatrixV2b(nbRows, nbColumns);
	}
	
	@Override
	public MatrixV2b createMatrix(double[][] data) {		
		return new MatrixV2b(data);
	}
}
