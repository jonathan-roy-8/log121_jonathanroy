import matrix.*;
import matrix2.*;
import badmatrix.BadMatrixUser;
 
public class Main{
	
  public static void main(String[] arg){
 
	  //testBadMatrix();
	  //testGoodMatrixA();
	  //testGoodMatrixB();
 
  }
  
  public static void testBadMatrix(){
	  BadMatrixUser user = new BadMatrixUser();
	  user.doComplexOperation();
  }
  
  public static void testGoodMatrixA(){
	  GoodMatrixUserV1 user1 = new GoodMatrixUserV1();
	  System.out.println("Solution du système d'équation, première implémentation de matrice");
  	  long start = System.currentTimeMillis();
	  
  	  user1.solveALinearSystemV1();
	  
	  long duree = System.currentTimeMillis() - start;
	  System.out.println("   Durée V1 : "+duree+" ms");
	  
	  GoodMatrixUserV2 user2 = new GoodMatrixUserV2();
	  System.out.println("Solution du système d'équation, deuxième implémentation de matrice");
	  start = System.currentTimeMillis();
	  
	  user2.solveALinearSystemV2();
	  
	  duree = System.currentTimeMillis() - start;
	  System.out.println("   Durée V2 : "+duree+" ms");
  }
  
  public static void testGoodMatrixB(){
	  BestMatrixUser user = new BestMatrixUser();
      double[][] d = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
      
      long start = System.currentTimeMillis();
	  user.solveALinearSystem(new MatrixV1b(d));
	  long duree = System.currentTimeMillis() - start;
	  System.out.println("   Durée V1 : "+duree+" ms");
	  
	  start = System.currentTimeMillis();
	  user.solveALinearSystem(new MatrixV2b(d));
	  duree = System.currentTimeMillis() - start;
	  System.out.println("   Durée V2 : "+duree+" ms");
  }
 
}