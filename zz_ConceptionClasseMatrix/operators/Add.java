// LOG121, groupe 01, automne 2012
// Patrice Boucher � copyright, 2012

package operators;
/**
 * This class implements an operation of addition
 */
public class Add implements Operator{

	@Override
	public double calculate(double na, double nb)
	{
		return na+nb;
	}
	@Override
	public String toString(){return "+";}
}
