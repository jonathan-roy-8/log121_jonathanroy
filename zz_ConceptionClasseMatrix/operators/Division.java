// LOG121, groupe 01, automne 2012
// Patrice Boucher � copyright, 2012
package operators;

/**
 * This class implements an operation of division
 */
public class Division implements Operator {

	@Override
	public double calculate(double element1, double element2) throws Exception{
		// TODO Auto-generated method stub
		if(element2==0)
			throw new Exception();
		return element1/element2;
	}
	@Override
	public String toString(){return "/";}
}
