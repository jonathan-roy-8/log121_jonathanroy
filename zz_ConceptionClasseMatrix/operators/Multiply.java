// LOG121, groupe 01, automne 2012
// Patrice Boucher � copyright, 2012
package operators;

/**
 * This class implements an operation of multiplication
 */
public class Multiply implements Operator{
	@Override
	public double calculate(double element1, double element2)
	{
		return element1*element2;
	}
	@Override
	public String toString(){return "*";}
}
