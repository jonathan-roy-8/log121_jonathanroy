// LOG121, groupe 01, automne 2012
// Patrice Boucher � copyright, 2012
package operators;

/**
 * This interface represents an operation
 */
public interface Operator {
	
	/**
	 * Process the operation
	 * @param na number one
	 * @param nb number two
	 * @return the result
	 * @throws Exception if the operation can not be processed
	 */
   double calculate(double na, double nb) throws Exception;
   
   /**
    * Represent the operation in a String
    * @return representation of the operation 
    */
   String toString();
}
