package matrix;

/**
 * Exemple d'un utilisateur de MatrixV1
 * @author Patrice Boucher
 */
public class GoodMatrixUserV2 {

	/**
	 * Trouve la solution (la valeur du vecteur x = A⁻¹Y) du système linéaire
	 *    Y = A*x
	 *    avec A = [1 2 3 ; 4 5 6 ; 9 1 3] et Y = [1 2 3]'
	 */
	public void solveALinearSystemV2(){
	       double[][] d = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
	       MatrixV2 A = new MatrixV2(d);
		   System.out.println("v = [1 2 3]' ");
	       double [][] dv = {{1},{2},{3}};
	       MatrixV2  v  = new MatrixV2(dv);
	       System.out.println("x = A.solve(v)");
	       MatrixV2  x = A.solve(v);
	       x.show();
	       System.out.println();  
	}
	
	/**
	 * Test la plupart des opérations matricielles fournit par MatrixV1
	 */
	public void doManyOperationsV2()
	{
       double[][] d = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
       MatrixV2 A = new MatrixV2(d);
       System.out.println("A");
       A.show();        
       System.out.println();
 
       A.swap(1, 2);
       System.out.println("Swap 1 <-> 2");
       A.show(); 
       System.out.println();

       MatrixV2 B = A.transpose();
       System.out.println("B = A^t");
       B.show(); 
       System.out.println();

       MatrixV2 C = MatrixV2.identity(3);
       System.out.println("C = Identity");
       C.show(); 
       System.out.println();

       System.out.println("A+B");
       A.plus(B).show();
       System.out.println();

       System.out.println("B*A");
       B.times(A).show();
       System.out.println();

       // shouldn't be equal since AB != BA in general  
       System.out.println("B==A");
       System.out.println(A.times(B).eq(B.times(A)));
       System.out.println();

       System.out.println("v =  ");
       double [][] dv = {{1},{2},{3}};
       MatrixV2 v = new MatrixV2(dv);
       v.show();
       System.out.println();

       System.out.println("x = A.solve(v)");
       MatrixV2 x = A.solve(v);
       x.show();
       System.out.println();

       System.out.println("Ax");
       A.times(x).show();
       }
}
