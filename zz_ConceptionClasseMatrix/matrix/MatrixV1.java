
package matrix;
import java.util.ArrayList;
import operators.*;


/**
 * Cette classe matricielle apporte des améliorations à BadMatrix :
 *   - Ses attributs son privés
 *   - Ajout d'accesseurs et mutateurs pratiques
 *   - Supporte des opérations matricielles de base
 * 
 * This entire class is based on the code on:
 *    http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
 * Some modifications of implementations are :
 * -  Usage of ArrayList to contain elements 
 * -  Add a method: operationEE that allows any element to element operations
 * -  Add javadoc comments
 * 
 * @author Patrice Boucher
 */
public class MatrixV1 {

private ArrayList<ArrayList<Double> > matrix;
private int nbRows;
private int nbColumns;
	
	/**
	 * Constructor
	 * @param nbRows number of rows
	 * @param nbColumns number of columns
	 */
	public MatrixV1(int nbRows, int nbColumns){
		this.nbRows = nbRows;
		this.nbColumns = nbColumns;
		int i,j;
		matrix = new ArrayList<ArrayList<Double> >();
		ArrayList<Double> row;
		for(i=0; i< nbRows ; ++i){
			row = new ArrayList<Double>();
			for(j=0 ; j< nbColumns ; ++j)
				row.add(0.0);
			matrix.add(row);
		}
	}
	

	/**
	 * Create matrix based on 2d array
	 * @param data
	 */
    public MatrixV1(double[][] data) {
        nbRows = data.length;
        nbColumns = data[0].length;
        matrix = new ArrayList<ArrayList<Double> >();
		ArrayList<Double> row;
		int i,j;
		for(i=0; i< nbRows ; ++i){
			row = new ArrayList<Double>();
			for(j=0 ; j< nbColumns ; ++j)
				row.add(data[i][j]);
			matrix.add(row);
		}
    }
	
    /**
     * Copy constructor
     * @param A the matrix to copy
     */
    private MatrixV1(MatrixV1 A) { 
    	this.nbRows = A.getSizeRow();
		this.nbColumns = A.getSizeColumn();
		int i,j;
		matrix = new ArrayList<ArrayList<Double> >();
		ArrayList<Double> row;
		for(i=0; i< nbRows ; ++i){
			row = new ArrayList<Double>();
			for(j=0 ; j< nbColumns ; ++j)
				row.add(A.get(i,j));
			matrix.add(row);
		}
    }
	
    /**
     * Create and return a random M-by-N matrix with values between 0 and 1
     * @param nbRows index of line
     * @param nbColumns index of column
     * @return a random matrix
     */
    public static MatrixV1 random(int nbRows, int nbColumns) {
    	MatrixV1 A = new MatrixV1(nbRows, nbColumns);
        for (int i = 0; i < nbRows; i++)
            for (int j = 0; j < nbColumns; j++)
                A.set(i,j, Math.random());
        return A;
    }
	
	/**
	 * Accessor
	 * @precondition 0 <= irow < this.getSizeRow();
	 * @precondition 0 <= icol < this.getSizeColumn();  
	 * @param irow index of line
	 * @param icol index of column
	 * @return selement
	 */
	public double get(int irow, int icol){
		return matrix.get(irow).get(icol);
	}
	
	/**
	 * Set an matrix's element
	 * @param irow index of line
	 * @param icol index of column
	 * @param element
	 */
	public void set(int irow, int icol, double value){
		matrix.get(irow).set( icol, value);
	}
	
	/**
	 * @return number of rows
	 */
	public int getSizeRow(){
		return nbRows;
	}
	
	/**
	 * 
	 * @return number of columns
	 */
	public int getSizeColumn(){
		return nbColumns;
	}
	

	/**
	 * Return a matrix obtained from applying an operator between the elements of this matrix
	 * with those of a second matrix
	 * @param matrix2 second matrix
	 * @param op operator
	 * @return matrix
	 * @throws Exception
	 */
	public MatrixV1 operateEE(MatrixV1 matrix2, Operator op) throws Exception{
		if (this.nbRows != matrix2.nbRows || this.nbColumns != matrix2.nbColumns) 
			throw new RuntimeException("Illegal matrix dimensions.");
		MatrixV1 matrix = new MatrixV1(nbRows,nbColumns);
		int i,j;
		for(i=0 ; i < nbRows ; ++i){
			for(j=0 ; j < nbColumns ; ++j){
				matrix.set(i, j, op.calculate(this.get(i, j), matrix2.get(i, j)));
			}
		}
		return matrix;
	}

 
	/**
	 * Create and return the N-by-N identity matrix
	 * @param inbColumns
	 * @return matrix
	 */
    public static MatrixV1 identity(int inbColumns) {
        MatrixV1 I = new MatrixV1(inbColumns, inbColumns);
        for (int i = 0; i < inbColumns; i++)
            I.set(i,i, 1);
        return I;
    }

    
    /**
     * 
     * @param z index of the row
     * @return row 
     */
    private ArrayList<Double> getRow(int z){
    	return matrix.get(z);
    }
    
    /**
     * Swap rows y and z
     * @param y row 1
     * @param z row 2
     */
    public void swap(int y, int z) {
        
    	MatrixV1 tmp= new MatrixV1(this);
    	matrix = new ArrayList<ArrayList<Double> >();
    	int i;
    	ArrayList<Double> row;
    	for(i=0; i< nbRows ; ++i){
    		if(i==y)
     		  row = tmp.getRow(z);
    		else if(i==z)
    		  row = tmp.getRow(y);
    		else
    		  row = tmp.getRow(i);
			matrix.add(row);
		}
    }

    /**
     * Create and return the transpose of the invoking matrix
     * @return transposed matrix
     */
    public MatrixV1 transpose() {
        MatrixV1 A = new MatrixV1(nbColumns, nbRows);
        for (int i = 0; i < nbRows; i++)
            for (int j = 0; j < nbColumns; j++)
                A.set(j,i, this.get(i,j));
        return A;
    }
    

    /**
     * Add this matrix to a second matrix
     * @param B second matrix
     * @return C = this + B
     */
    public MatrixV1 plus(MatrixV1 B) {
        MatrixV1 C=null;
		try {
			C = this.operateEE(B, new Add());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return C;
    }


    /**
     * Subtract this matrix to a second matrix
     * @param B second matrix
     * @return C = this - B
     */
    public MatrixV1 minus(MatrixV1 B) {
    	MatrixV1 A = this;
        if (B.nbRows != A.nbRows || B.nbColumns != A.nbColumns) throw new RuntimeException("Illegal matrix dimensions.");
        MatrixV1 C = null;
		try {
			C = this.operateEE(B, new Subtract());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return C;
    }

    /**
     * Does A = B exactly?
     * @param B
     * @return A = B exactly?
     */
    public boolean eq(MatrixV1 B) {
    	MatrixV1 A = this;
        if (B.nbRows != A.nbRows || B.nbColumns != A.nbColumns) 
        	throw new RuntimeException("Illegal matrix dimensions.");
        for (int i = 0; i < nbRows; i++)
            for (int j = 0; j < nbColumns; j++)
                if (A.get(i,j) != B.get(i,j)) return false;
        return true;
    }
	
    /**
     * Multiply this matrix with a second matrix
     * @param B second matrix
     * @return  C = this * B
     */
    public MatrixV1 times(MatrixV1 B) {
    	MatrixV1 A = this;
        if (A.nbColumns != B.nbRows) throw new RuntimeException("Illegal matrix dimensions.");
        MatrixV1 C = new MatrixV1(A.nbRows, B.nbColumns);
        for (int i = 0; i < C.nbRows; i++)
            for (int j = 0; j < C.nbColumns; j++)
                for (int k = 0; k < A.nbColumns; k++)
                    C.set(i,j, C.get(i, j)+(A.get(i,k) * B.get(k,j)));
        return C;
    }

    /**
     * Solve a linear system Y= A*x, with A=this matrix
     * @param Y
     * @return x = this^-1 B, assuming A is square and has full rank
     */
    public MatrixV1 solve(MatrixV1 Y) {
        if (nbRows != nbColumns || Y.nbRows != nbColumns || Y.nbColumns != 1)
            throw new RuntimeException("Illegal matrix dimensions.");

        // create copies of the data
        MatrixV1 A = new MatrixV1(this);
        MatrixV1 b = new MatrixV1(Y);

        // Gaussian elimination with partial pivoting
        for (int i = 0; i < nbColumns; i++) {

            // find pivot row and swap
            int max = i;
            for (int j = i + 1; j < nbColumns; j++)
                if (Math.abs(A.get(j,i)) > Math.abs(A.get(max,i)))
                    max = j;
            A.swap(i, max);
            b.swap(i, max);

            // singular
            if (A.get(i,i) == 0.0) throw new RuntimeException("Matrix is singular.");

            // pivot within b
            for (int j = i + 1; j < nbColumns; j++)
                b.set(j,0,b.get(j, 0) - b.get(i,0) * A.get(j,i) / A.get(i,i));

            // pivot within A
            for (int j = i + 1; j < nbColumns; j++) {
                double m = A.get(j,i) / A.get(i,i);
                for (int k = i+1; k < nbColumns; k++) {
                    A.set(j,k, A.get(j, k) - A.get(i,k) * m);
                }
                A.set(j,i, 0.0);
            }
        }
     // back substitution
        MatrixV1 x = new MatrixV1(nbColumns, 1);
        for (int j = nbColumns - 1; j >= 0; j--) {
            double t = 0.0;
            for (int k = j + 1; k < nbColumns; k++)
                t += A.get(j,k) * x.get(k,0);
            x.set(j,0, (b.get(j,0) - t) / A.get(j,j));
        }
        return x;
    }
    
    
    /**
     * Print matrix to standard output
     */
    public void show() {
        for (int i = 0; i < nbRows; i++) {
            for (int j = 0; j < nbColumns; j++) 
                System.out.printf("%9.4f ", get(i,j));
            System.out.println();
        }
    }

}
