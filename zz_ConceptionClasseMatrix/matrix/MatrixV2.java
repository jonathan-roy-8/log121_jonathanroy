
// Ce fichier, qui contient une classe pour effectuer des opérations matricielles,
// est emprunté de Robert Sedgewick and Kevin Wayne sur le cite internet: 
// http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
// - Le nom de la classe Matrix a été changé pour MatrixV2
// - Un accesseur et un mutateur a été ajouté
// - La méthode swap est mise publique

/*************************************************************************
 *  Compilation:  javac Matrix.java
 *  Execution:    java Matrix
 *
 *  A bare-bones immutable data type for M-by-N matrices.
 *
 *************************************************************************/
package matrix;

/**
 * Cette classe matricielle apporte des améliorations à BadMatrix :
 *   - Ses attributs son privés
 *   - Ajout d'accesseurs et mutateurs pratiques
 *   - Supporte des opérations matricielles de base
 * 
 * This entire class is based on the code on:
 *    http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
 * Some modifications of implementations are :
 * -  Usage of ArrayList to contain elements 
 * -  Add a method: operationEE that allows any element to element operations
 * -  Add javadoc comments
 * 
 * @author Patrice Boucher
 */
public class MatrixV2 {
    private final int M;             // number of rows
    private final int N;             // number of columns
    private double[][] data;   // M-by-N array

    /**
	 * Constructor
	 * @param M number of lines 
	 * @param N number of columns
	 */
    public MatrixV2(int M, int N) {
        this.M = M;
        this.N = N;
        data = new double[M][N];
    }

    /**
	 * Create matrix based on 2d array
	 * @param data table of data
	 */
    public MatrixV2(double[][] data) {
        M = data.length;
        N = data[0].length;
        this.data = new double[M][N];
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                    this.data[i][j] = data[i][j];
    }

    /**
     * Copy constructor
     * @param A the matrix to copy
     */
    private MatrixV2(MatrixV2 A) { this(A.data); }

	/**
	 * Accessor
	 * @precondition 0 <= irow < this.getSizeRow();
	 * @precondition 0 <= icol < this.getSizeColumn();  
	 * @param irow index of line
	 * @param icol index of column
	 * @return element
	 */
	public double get(int irow, int icol){
		return data[irow][icol] ;
	}
	
	/**
	 * Set an matrix's element
	 * @param irow row's index
	 * @param icol column's index
	 * @param element
	 */
	public void set(int irow, int icol, double value){
		data[irow][icol] = value;
	}
	
	/**
	 * @return number of lines
	 */
	public int getSizeRow(){
		return M;
	}
	
	/**
	 * 
	 * @return number of columns
	 */
	public int getSizeColumn(){
		return N;
	}
    
	/**
     * Create and return a random M-by-N matrix with values between 0 and 1
     * @param nbRows index of line
     * @param nbColumns index of column
     * @return a random matrix
     */
    public static MatrixV2 random(int M, int N) {
        MatrixV2 A = new MatrixV2(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[i][j] = Math.random();
        return A;
    }

    /**
	 * Create and return the N-by-N identity matrix
	 * @param inbColumns
	 * @return matrix
	 */
    public static MatrixV2 identity(int N) {
        MatrixV2 I = new MatrixV2(N, N);
        for (int i = 0; i < N; i++)
            I.data[i][i] = 1;
        return I;
    }

    /**
     * Swap rows y and z
     * @param y row 1
     * @param z row 2
     */
    public void swap(int i, int j) {
        double[] temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

    /**
     * Create and return the transpose of the invoking matrix
     * @return transposed matrix
     */
    public MatrixV2 transpose() {
        MatrixV2 A = new MatrixV2(N, M);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[j][i] = this.data[i][j];
        return A;
    }

    /**
     * Add this matrix to a second matrix
     * @param B second matrix
     * @return C = this + B
     */
    public MatrixV2 plus(MatrixV2 B) {
        MatrixV2 A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        MatrixV2 C = new MatrixV2(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                C.data[i][j] = A.data[i][j] + B.data[i][j];
        return C;
    }


    /**
     * Subtract this matrix to a second matrix
     * @param B second matrix
     * @return C = this - B
     */
    public MatrixV2 minus(MatrixV2 B) {
        MatrixV2 A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        MatrixV2 C = new MatrixV2(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                C.data[i][j] = A.data[i][j] - B.data[i][j];
        return C;
    }

    /**
     * Does A = B exactly?
     * @param B
     * @return A = B exactly?
     */
    public boolean eq(MatrixV2 B) {
        MatrixV2 A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (A.data[i][j] != B.data[i][j]) return false;
        return true;
    }

    /**
     * Multiply this matrix with a second matrix
     * @param B second matrix
     * @return  C = this * B
     */
    public MatrixV2 times(MatrixV2 B) {
        MatrixV2 A = this;
        if (A.N != B.M) throw new RuntimeException("Illegal matrix dimensions.");
        MatrixV2 C = new MatrixV2(A.M, B.N);
        for (int i = 0; i < C.M; i++)
            for (int j = 0; j < C.N; j++)
                for (int k = 0; k < A.N; k++)
                    C.data[i][j] += (A.data[i][k] * B.data[k][j]);
        return C;
    }

    /**
     * Solve a linear system Y= A*x, with A=this matrix
     * @param Y
     * @return x = this^-1 B, assuming A is square and has full rank
     */
    public MatrixV2 solve(MatrixV2 rhs) {
        if (M != N || rhs.M != N || rhs.N != 1)
            throw new RuntimeException("Illegal matrix dimensions.");

        // create copies of the data
        MatrixV2 A = new MatrixV2(this);
        MatrixV2 b = new MatrixV2(rhs);

        // Gaussian elimination with partial pivoting
        for (int i = 0; i < N; i++) {

            // find pivot row and swap
            int max = i;
            for (int j = i + 1; j < N; j++)
                if (Math.abs(A.data[j][i]) > Math.abs(A.data[max][i]))
                    max = j;
            A.swap(i, max);
            b.swap(i, max);

            // singular
            if (A.data[i][i] == 0.0) throw new RuntimeException("Matrix is singular.");

            // pivot within b
            for (int j = i + 1; j < N; j++)
                b.data[j][0] -= b.data[i][0] * A.data[j][i] / A.data[i][i];

            // pivot within A
            for (int j = i + 1; j < N; j++) {
                double m = A.data[j][i] / A.data[i][i];
                for (int k = i+1; k < N; k++) {
                    A.data[j][k] -= A.data[i][k] * m;
                }
                A.data[j][i] = 0.0;
            }
        }

        // back substitution
        MatrixV2 x = new MatrixV2(N, 1);
        for (int j = N - 1; j >= 0; j--) {
            double t = 0.0;
            for (int k = j + 1; k < N; k++)
                t += A.data[j][k] * x.data[k][0];
            x.data[j][0] = (b.data[j][0] - t) / A.data[j][j];
        }
        return x;
   
    }

    /**
     * Print matrix to standard output
     */
    public void show() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) 
                System.out.printf("%9.4f ", data[i][j]);
            System.out.println();
        }
    }
}


//Copyright © 2000–2011, Robert Sedgewick and Kevin Wayne.
//Last updated: Wed Feb 9 09:20:16 EST 2011. 