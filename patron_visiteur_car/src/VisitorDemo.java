/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Patrice Boucher: 
 * - Ajout de commentaires javadoc, 
 * - (18 avril 2013) Modification du code: la méthode accept de l'automobile est appelée. 
 */
 
import visitor.*;
import element.*;
	 
/**
 * Classe de d�mo
 */
public class VisitorDemo
{
    static public void main(String[] args)
    {
    	System.out.println("Visitor Demo");
        Car car = new Car(); // Cr�ation d'une automobile
        car.accept(new CarNameVisitor()); // Affiche les �l�ments de l'automobile
        car.accept( new CarActionVisitor()); // Affiche des actions relatives aux �l�ments de l'automobile
    }
}