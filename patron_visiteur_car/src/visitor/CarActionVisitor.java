/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Commentaires javadoc, Patrice Boucher
 */

package visitor;
import element.*;

/**
 * Affiche une action relative � chaque �l�ment d'automobile
 */
public class CarActionVisitor implements CarVisitor
{
	
	@Override
    public void visitWheel(Wheel wheel)
    {
        System.out.println("Kicking my "+ wheel.getName());
    }
	@Override 
    public void visitEngine(Engine engine)
    {
        System.out.println("Starting my engine");
    }
	@Override
    public void visitBody(Body body)
    {
        System.out.println("Moving my body");
    }
	@Override 
    public void visitCar(Car car)
    {
        System.out.println("\nStarting my car");
        for(CarElement carElement : car.getElements())
        {
           carElement.accept(this);
        }
        System.out.println("Started car");
    }
}