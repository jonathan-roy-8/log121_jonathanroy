/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Commentaires javadoc, Patrice Boucher
 */

package visitor;
import element.*;

/**
 * D�finit l'interface d'un "visiteur" d'�l�ment d'automobile
 */
public interface CarVisitor
{
	/**
	 * Visite une roue
	 * @param wheel la roue
	 */
    void visitWheel(Wheel wheel);
    
    /**
     * Visite un moteur
     * @param engine le moteur
     */
    void visitEngine(Engine engine);
    
    /**
     * Visite le corps de l'automobile
     * @param body le corps
     */
    void visitBody(Body body);
    
    /**
     * Visite une automobile
     * @param car l'automobile
     */
    void visitCar(Car car);
}