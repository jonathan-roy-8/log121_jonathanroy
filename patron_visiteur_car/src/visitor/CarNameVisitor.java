/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Commentaires javadoc, Patrice Boucher
 */

package visitor;
import element.*;

/**
 *  Classe qui affiche les �l�ments d'une l'automobile dans une console
 */
public class CarNameVisitor implements CarVisitor
{
	@Override
    public void visitWheel(Wheel wheel)
    {
        System.out.println("Visiting "+ wheel.getName() + " wheel");
    }
	@Override 
    public void visitEngine(Engine engine)
    {
        System.out.println("Visiting engine");
    }
	@Override 
    public void visitBody(Body body)
    {
        System.out.println("Visiting body");
    }
	@Override 
    public void visitCar(Car car)
    {
        System.out.println("\nVisiting car");
        for(CarElement element : car.getElements())
        {
           element.accept(this);
        }
        System.out.println("Visited car");
        
        
        // Façon sans utiliser la méthode accept
        /*System.out.println("\nVisiting car");
        for(CarElement element : car.getElements())
        {
           if(element instanceof Wheel)
        	   visitWheel((Wheel)element);
           else if(element instanceof Body)
        	   visitBody((Body) element);
           else if(element instanceof Engine)
        	   visitEngine((Engine) element);
        }
        System.out.println("Visited car");*/
    }
}