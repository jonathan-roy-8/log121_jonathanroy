/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Commentaires javadoc, Patrice Boucher
 */

package element;
import visitor.*;

/**
 * Interface d'un �l�ment d'automobile
 * Chaque �l�ment peut accepter d'�tre trait� par un "visitor". 
 * Est-ce que cela respecte l'encapsulation? non et oui:
 *  - Non: bris de l'encapsulation au sens que ce n'est pas le CarElement qui effectue ses op�rations
 *  - Oui: maintient de l'encapsulation au sens que le CarElement d�cide de d�l�guer, accepte, le traitement 
 *    effectu� par le visiteur. Sa conception impose, de mani�re claire, que le traitement soit effectu� par
 *    une classe sp�cialis�e, ici "CarElementVisitor". 
 */
public interface CarElement
{
	 /**
	  * M�thode � d�finir par les classes impl�mentant CarElement
	  * @param visitor objet qui effectue un traitement sur le CarLement
	  */
	 void accept(CarVisitor visitor);

}