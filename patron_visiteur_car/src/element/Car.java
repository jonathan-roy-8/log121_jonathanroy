/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Patrice Boucher Commentaires javadoc, ajout de la méthode accept (18 avril 2013)
 */
package element;

import visitor.CarVisitor;


public class Car
{
   CarElement[] elements; // Collections d'�l�ment d'automobile
 
   /**
    * @return un tableau d'�l�ments d'automobile
    */
   public CarElement[] getElements()
   {
      return elements.clone(); // Retourne une copie du tableau de r�f�rences.
   }
	 
   /**
    * Constructeur
    */
   public Car()
   {
	 // Pour faire simple, on suppose qu'une automobile comporte 4 roues,
	 // un corps principal et un moteur
     this.elements = new CarElement[]{
         new Wheel("front left"),
         new Wheel("front right"),
         new Wheel("back left"),
         new Wheel("back right"),
         new Body(),
         new Engine()
         };
	}
   
   public void accept(CarVisitor carVisitor){
	   carVisitor.visitCar(this);
   }
}