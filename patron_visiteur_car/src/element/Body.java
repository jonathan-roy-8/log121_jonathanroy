/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Commentaires javadoc, Patrice Boucher
 */
package element;
import visitor.*;

/**
 * Repr�sente la carosserie de l'automobile
 */
public class Body implements CarElement
{
	@Override
    public void accept(CarVisitor visitor)
    {
      visitor.visitBody(this);
    }
}