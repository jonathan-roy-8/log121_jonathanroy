/*
 * Cet exemple est tir� int�gralement du site internet suivant:
 * http://fr.wikibooks.org/wiki/Patrons_de_conception/Visiteur
 * Il pr�sente un exemple du patron Visiteur
 * Commentaires javadoc, Patrice Boucher
 */
package element;
import visitor.*;

/**
 * Une roue
 */
public class Wheel implements CarElement
{
	    private String name;
	 
	    /**
	     * Constructeur
	     * @param name nom de la roue
	     */
	    Wheel(String name)
	    {
	        this.name = name;
	    }
	 
	    /**
	     * @return nom de la roue
	     */
	    public String getName()
	    {
	        return this.name;
	    }
	 
	    @Override
	    public void accept(CarVisitor visitor)
	    {
	        visitor.visitWheel(this);
	    }
}