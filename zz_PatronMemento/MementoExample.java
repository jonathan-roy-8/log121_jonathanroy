
/** Ce fichier est construit intégralement à partir de l'exemple du site internet suivant:
 *  http://fr.wikibooks.org/wiki/Patrons_de_conception/M%C3%A9mento
 *  Les noms des méthodes ont été modifiés pour se conformer à ceux utilisés 
 *  dans le schéma présenté au cours. 
 */

import java.util.*;
 
class Originator
{
    private String state;
 
    public void set(String state)
    {
        System.out.println("Originator : état affecté à : "+state);
        this.state = state;
    }
 
    public Object createMemento()
    {
        System.out.println("Originator : sauvegardé dans le mémento.");
        return new Memento(state);
    }
 
    public void setMemento(Object m)
    {
        if (m instanceof Memento)
        {
            Memento memento = (Memento)m;
            state = memento.getState();
            System.out.println("Originator : État après restauration : "+state);
        }
    }
 
    /**
     * Classe interne et privée
     */
    private static class Memento
    {
        private String state; // état à sauvegarder
 
        public Memento(String stateToSave) { state = stateToSave; }
        public String getState() { return state; }
    }
}
 
class Caretaker
{
    private ArrayList states = new ArrayList();
 
    public void addMemento(Object m) { states.add(m); }
    public Object getMemento(int index) { return states.get(index); }
}
 
class MementoExample
{
    public static void main(String[] args)
    {
        Caretaker caretaker = new Caretaker();
 
        Originator originator = new Originator();
        originator.setMemento("State1");
        originator.setMemento("State2");
        caretaker.addMemento( originator.createMemento() );
        originator.setMemento("State3");
        caretaker.addMemento( originator.createMemento() );
        originator.setMemento("State4");
 
        originator.setMemento( caretaker.getMemento(1) );
    }
}
