/*
   auteur: S�bastien Adam

*/

import java.io.*;

public class Logger 
{
    private static Logger logger = null;

    private Logger() 
    {
        logger = this;
    }

    public void logMsg(String message, String fileName) 
    {
        try 
        {
        	System.out.println(logger);
            PrintStream ps = new PrintStream(new FileOutputStream(fileName, true));
            ps.println(message);
            ps.close();
        }
        catch (IOException ie) 
        {
            ie.printStackTrace();
        }
    }

    public static Logger getLogger()  {
        if (logger == null) { 
        	System.out.println("Create Logger Instance");
        	logger = new Logger(); 
        } else {
        	System.out.println("Use existing Logger Instance");
        }
        
        return logger;
    }
}
