/*
   auteur: S�bastien Adam

 */

public class TestSingleton {

	public static void main(String[] args) {
		System.out.println("TestSingleton: main");
		
		Logger.getLogger().logMsg("juste un test", "log.txt");

		Logger.getLogger().logMsg("should use existing instance", "log.txt");
	}

}
