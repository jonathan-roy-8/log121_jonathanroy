/*
   auteur: Sébastien Adam

   source: http://csis.pace.edu/~bergin/patterns/strategydecorator.html

*/

import java.util.Vector;

public class AndStrategyComposite implements CheckStrategy {
	private java.util.Vector tests = new Vector();

	public void addStrategy(CheckStrategy s) {
		tests.addElement(s);
	}

	public boolean check(String s) {
		java.util.Enumeration e = tests.elements();
		while (e.hasMoreElements()) {
			CheckStrategy strategy = (CheckStrategy) e.nextElement();
			if (!strategy.check(s))
				return false;
		}
		return true;
	}
}
