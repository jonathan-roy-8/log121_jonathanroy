/*
   auteur: S�bastien Adam

   source: http://csis.pace.edu/~bergin/patterns/strategydecorator.html

*/


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class TestComposite {
	public static void printWhen(String filename, CheckStrategy which)
			throws IOException {
		BufferedReader infile = new BufferedReader(new FileReader(filename));
		String buffer = null;
		while ((buffer = infile.readLine()) != null) {
			StringTokenizer words = new StringTokenizer(buffer);
			while (words.hasMoreTokens()) {
				String word = words.nextToken();
				if (which.check(word))
					System.out.println(word);
			}
		}
	}
	public static void main(String[] args) {
		try {
			System.out.println("TestComposite");
			System.out.println("Entrer le nom du ficheir à vérifier: (test.txt)");	
			Scanner clavier = new Scanner(System.in);
			String nomFic = clavier.next();
			if (nomFic.length() == 0) {
				nomFic = "test.txt";
			}
			
			System.out.println("Create a composite of stragety");
			AndStrategyComposite longWordsThatStartWithT = new AndStrategyComposite();
			
			System.out.println("add strategy LongerThanN(5)");
			longWordsThatStartWithT.addStrategy(new LongerThanN(5));
			
			System.out.println("add strategy StartWithT()");	
			longWordsThatStartWithT.addStrategy(new StartWithT());	
			
			System.out.println("Print content of file filtered with composite of strategy");
			printWhen(nomFic, longWordsThatStartWithT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
