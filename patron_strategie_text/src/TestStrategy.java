/*
   auteur: S�bastien Adam

   source: http://csis.pace.edu/~bergin/patterns/strategydecorator.html

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class TestStrategy {
	public static void printWhen(String filename, CheckStrategy which)
			throws IOException {
		BufferedReader infile = new BufferedReader(new FileReader(filename));
		String buffer = null;
		while ((buffer = infile.readLine()) != null) {
			StringTokenizer words = new StringTokenizer(buffer);
			while (words.hasMoreTokens()) {
				String word = words.nextToken();
				if (which.check(word))
					System.out.println(word);
			}
		}
	}
	public static void main(String[] args) {
		try {
			System.out.println("TestStrategy: main");
			System.out.println("Entrer le nom du ficheir à vérifier: (test.txt)");	
			Scanner clavier = new Scanner(System.in);
			String nomFic = clavier.next();
			
			System.out.println("Strategie StartWithT ----------");
			printWhen(nomFic, new StartWithT());
			
			System.out.println("Strategie Palindrome -----------");
			printWhen(nomFic, new Palindrome());
			
			System.out.println("Strategie LongerThanN(5) ---------");
			printWhen(nomFic, new LongerThanN(5));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
