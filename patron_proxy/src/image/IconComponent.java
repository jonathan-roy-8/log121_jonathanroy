
package image;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 * Adapte un Icon en JComponent
 * @author patrice
 */
public class IconComponent extends JComponent{

	private static final long serialVersionUID = -8830927664442804569L;
	private Icon icon = null; 
	private Dimension dimension; 
	
	/**
	 * Constructeur
	 * @param icon le nom de l'image
	 */
	public IconComponent(String name, int width, int height){
		dimension = new Dimension(width, height);
		Image image;
		Image image2=null;
		try {
			image = ImageIO.read(new File(name));
			image2 = image.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.icon = new ImageIcon(image2); //icon;
	}
	
	@Override
	public void paintComponent(Graphics g){
		long start = System.currentTimeMillis();
		icon.paintIcon(this, g, 0, 0);
		long duree = System.currentTimeMillis() - start;
    	System.out.println("Changement de photo: "+duree+" ms");
	}
	
	@Override
	public Dimension getPreferredSize(){
		return dimension;
	}
}
