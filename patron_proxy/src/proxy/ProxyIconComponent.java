
package proxy;

import image.IconComponent;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.*;

/**
 * Proxy d'un IconComponent: cr�e le IconComponant lors de sa premi�re visite
 * @author Patrice Boucher
 */
public class ProxyIconComponent extends JComponent
{
	private static final long serialVersionUID = 2488100092551524198L;
	private String filename;
	private Dimension dimension;
    private IconComponent iconc=null;
 
    /**
     * Constructeur
     * @param filename nom du fichier de l'image
     * @param width largeur de l'image
     * @param height hauteur de l'image
     */
    public ProxyIconComponent(String filename, int width, int height)
    {
        this.filename = filename;
        dimension = new Dimension(width,height);
    }
 
	@Override
	public void paintComponent(Graphics g){
		long start = System.currentTimeMillis();
        if (iconc == null)
        {
            iconc = new IconComponent(filename, dimension.width, dimension.height); // Chargement sur demande seulement
        }
       // this.setPreferredSize(image.getPreferredSize());
        iconc.paintComponent(g);      
		long duree = System.currentTimeMillis() - start;
    	System.out.println("Changement de photo: "+duree+" ms");
	}
	
	@Override
	public Dimension getPreferredSize(){
		   return dimension;
	}
}
