
package proxy;

import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.*;

/**
 * Proxy d'un IconComponent: cr�e le IconComponant lors de sa premi�re visite
 * @author Patrice Boucher
 */
public class PrivProxyIconComponent extends JComponent
{
	private static final long serialVersionUID = 2488100092551524198L;
    private ProxyIconComponent ppic=null;
 
    /**
     * Constructeur
     * @param nameClient nom de celui qui veut regarder l'album
     * @param fileName nom du fichier de l'image
     * @param width largeur de l'image
     * @param height hauteur de l'image
     */
    public PrivProxyIconComponent(String nameClient, String fileName, int width, int height)
    {
        if(isVIP(nameClient))
        	ppic = new ProxyIconComponent(fileName, width, height);
        else
        	ppic = new ProxyIconComponent("src/accesinterdit.png", width, height);
    }
 
	@Override
	public void paintComponent(Graphics g){
        ppic.paintComponent(g);      
	}
	
	@Override
	public Dimension getPreferredSize(){
		   return (Dimension) ppic.getPreferredSize();
	}
	
	/**
	 * Regarde si un nom est dans la liste VIP qui peut acc�der � l'album
	 * @param nameClient
	 * @return
	 */
	private boolean isVIP(String nameClient){
		Scanner sc;
		try {
			sc = new Scanner(new File("secretFile.album"));
			while (sc.hasNext()) {
		        String nameFile = sc.next();
		        if(nameClient.compareTo(nameFile)==0){
		        	sc.close();
		        	return true;
		        }
		      } 
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
}
