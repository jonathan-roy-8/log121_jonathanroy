
package albums;

import image.IconComponent;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;

/**
 * Premi�re version de l'album qui utilise directement IconComponent
 * @author Patrice Boucher
 */
public class CustumAlbumV1 extends JFrame{


	private static final long serialVersionUID = 811046520599509801L;
	private final int ALBUM_WIDTH = 600;
	private final int ALBUM_HEIGHT = 400;
	ArrayList<JComponent> pictures;
	int indexPicture=-1;
	JPanel buttonPanel;
	
	/**
	 * Constructeur
	 */
	public CustumAlbumV1(String albumFileName) throws FileNotFoundException{

		pictures = new ArrayList<JComponent>();
		load(albumFileName);
		
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		final JButton previous= new JButton("precedent");
		buttonPanel.add(previous);
		previous.setVisible(false);
		final JFrame frame=this;
		
		final JButton next = new JButton("suivant");
		next.setVisible(true);
		buttonPanel.add(next);
		
		previous.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.remove(pictures.get(indexPicture));
				indexPicture = indexPicture-1;
				frame.add(pictures.get(indexPicture),BorderLayout.CENTER);
				if(indexPicture==0)
					previous.setVisible(false);
				else{
					previous.setVisible(true);
				}
				next.setVisible(true);
				frame.validate();
				frame.repaint();
			}
		});
		
		next.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.remove(pictures.get(indexPicture));
				indexPicture = indexPicture+1;
				frame.add(pictures.get(indexPicture),BorderLayout.CENTER);
				if(indexPicture==pictures.size()-1)
					next.setVisible(false);
				else{
					next.setVisible(true);
				}
				previous.setVisible(true);
				frame.validate();
				frame.repaint();
			}
		});
		
		this.setLayout(new BorderLayout());
		this.add(buttonPanel,BorderLayout.NORTH);
		this.add(pictures.get(0),BorderLayout.CENTER);
		indexPicture = 0;
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Charge l'album
	 * @param fileName nom du fichier contenant les noms des fichiers des photos
	 * @throws FileNotFoundException 
	 */
	private void load(String fileName) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(fileName));
		while (sc.hasNext()) {
	          String imfile = sc.next();
	          System.out.println(imfile);
	          JComponent image1 = new IconComponent(imfile,ALBUM_WIDTH,ALBUM_HEIGHT);
	          pictures.add(image1);
	       }
		sc.close();
		System.out.println(pictures.size()+" photos charg�es");
	}
}
