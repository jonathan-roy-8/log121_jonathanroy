
 
import java.io.FileNotFoundException;

import albums.CustumAlbumV1;
import albums.CustumAlbumV2;
import albums.CustumAlbumV3;

/**
 * Exemple d'application du patron proxy:
 *   Optimisation du temps de cr�ation d'un album de photos
 * 
 * @author Patrice Boucher
 *
 */

class ProxyExample
{
	/**
	 * Ex�cution de l'exemple: d�commentez l'album � tester
	 * @param args sans importance
	 */
    public static void main(String[] args)
    {
    	try {
//			createAlbum1("irlande.album");// toutes les images sont charg�es � la cr�ation de l'album
//			createAlbum2("irlande.album"); // chaque image est charg�e � sa premi�re visite
//			createAlbum3("Mona","irlande.album"); // Consultez "secretFile.album" pour savoir
			createAlbum3("yvan","irlande.album"); // Consultez "secretFile.album" pour savoir
			   // qui peut consulter l'album. Essayez aussi n'importe quel nom pour appr�cier l'effet
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
    
    /**
     * Cr�ation de l'album photo 1: toutes les images sont charg�es � la cr�ation de l'album
     * Effet de bord: le temps de cr�ation de l'album est affich� dans System.out
     * @throws FileNotFoundException 
     */
    private static void createAlbum1(String fileName) throws FileNotFoundException{
    	long start = System.currentTimeMillis();
    	CustumAlbumV1 album1 = new CustumAlbumV1(fileName);
    	long duree = System.currentTimeMillis() - start;
    	System.out.println("Cr�ation album1: "+duree+" ms");
    }
    
    /**
     * Cr�ation de l'album photo 2: chaque image est charg�e � sa premi�re visite
     * Effet de bord: le temps de cr�ation de l'album est affich� dans System.out
     * @param fileName fichier qui liste les fichiers des photos
     * @throws FileNotFoundException 
     */
    private static void createAlbum2(String fileName) throws FileNotFoundException{
    	long start = System.currentTimeMillis();
    	CustumAlbumV2 album2 = new CustumAlbumV2(fileName);
    	long duree = System.currentTimeMillis() - start;
    	System.out.println("Cr�ation album1: "+duree+" ms");
    }
    
    /**
     * Cr�ation de l'album photo 3: chaque image est charg�e � sa premi�re visite,
     * le nom du visiteur doit �tre dans une liste VIP pour avoir acc�s � l'album
     * Effet de bord: le temps de cr�ation de l'album est affich� dans System.out
     * @param name nom du visiteur
     * @param fileName fichier qui liste les fichiers des photos
     * @throws FileNotFoundException 
     */
    private static void createAlbum3(String name, String fileName) throws FileNotFoundException{
    	long start = System.currentTimeMillis();
    	CustumAlbumV3 album3 = new CustumAlbumV3(name, fileName);
    	long duree = System.currentTimeMillis() - start;
    	System.out.println("Cr�ation album1: "+duree+" ms");
    }
}
