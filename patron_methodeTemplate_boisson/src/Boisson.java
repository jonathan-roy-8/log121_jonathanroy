
public abstract class Boisson {
	
	public void preparer(){
		bouillirEau();
		infuser();
		verserTasse();
		ajouterCondiments();
	}
	
	private void bouillirEau(){
		System.out.println("L'eau chauffe");
	}
	
	protected abstract void infuser();

	private void verserTasse(){
		System.out.println("La boisson est versée dans la tasse");
	}
	
	protected abstract void ajouterCondiments();
}
