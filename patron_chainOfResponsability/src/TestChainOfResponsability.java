/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class TestChainOfResponsability {
	public static void main(String[] args) {

		Director d = new Director();
		VicePresident v = new VicePresident();
		President p = new President();

		d.setSuccessor(v);
		v.setSuccessor(p);

		Purchase purch = new Purchase(2034, 350.00, "Supplies");
		d.processRequest(purch);

		purch = new Purchase(2035, 32590.10, "Project X");
		d.processRequest(purch);

		purch = new Purchase(2036, 122100.00, "Project Y");
		d.processRequest(purch);
	}
}
