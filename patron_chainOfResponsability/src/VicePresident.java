/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class VicePresident extends Approver {
	public void processRequest(Purchase purchase) {
		if (purchase.getAmount() < 25000.0) {
			System.out.println(getClass().getName() + " approved request# "
					+ purchase.getNumber());
		} else if (successor != null) {
			successor.processRequest(purchase);
		}
	}
}
