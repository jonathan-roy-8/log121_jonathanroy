/*
   auteur: S�bastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class Director extends Approver {

	public void processRequest(Purchase p) {
		
		if (p.getAmount() < 10000.0) {
			System.out.println("If request# " + p.getNumber() +": " +  p.getAmount() +  "< 1000 then Director approve request# " + p.getNumber());
		} else if (successor != null) {
			System.out.println("If " + p.getAmount() +  "> 1000 then Director manager approve it");
					successor.processRequest(p);
		}
	}

}
