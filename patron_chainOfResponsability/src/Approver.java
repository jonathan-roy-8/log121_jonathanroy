/*
   auteur: S�bastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public abstract class Approver {
	protected Approver successor;

	public void setSuccessor(Approver successor) {
		System.out.println(this + " is successeded by " + successor);
		this.successor = successor;
	}

	public abstract void processRequest(Purchase purchase);
}
