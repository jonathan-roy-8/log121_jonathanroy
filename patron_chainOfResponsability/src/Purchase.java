/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class Purchase {
	private int number;

	private double amount;

	private String purpose;

	public Purchase(int number, double amount, String purpose) {
		this.number = number;
		this.amount = amount;
		this.purpose = purpose;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double value) {
		amount = value;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String value) {
		purpose = value;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int value) {
		number = value;
	}

}
