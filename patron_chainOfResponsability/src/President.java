/*
   auteur: S�bastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class President extends Approver {
	public void processRequest(Purchase purchase) {
		if (purchase.getAmount() < 100000.0) {	
			System.out.println("if request# " + purchase.getNumber() + ": " + purchase.getAmount() + " < 100000 " + getClass().getName() + " approved request# " + purchase.getNumber());
		} else {
			System.out.println("Request# " + purchase.getNumber()
					+ " requires an executive meeting!");
		}
	}
}
