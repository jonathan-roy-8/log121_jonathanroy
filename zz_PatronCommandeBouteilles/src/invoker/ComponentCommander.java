
package invoker;
 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
  
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
 

import commands.Command;
import commands.CommandHandler;

 

/**
 * Cette classe fournit une fenêtre graphique (JComponent) pour effectuer des commandes sur 
 * une sous-fenêtre (JComponent) via des boutons. 
 * @author Patrice Boucher
 */
public class ComponentCommander extends JComponent{
	
	private static final long serialVersionUID = 6281984331203746669L;

	/**
	 * Constructeur
	 * @param compToCommand sous-fenêtre sur laquelle on veut effectuer des commandes
	 */
	public ComponentCommander(JComponent compToCommand){
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.add(compToCommand);
	}
	
	/**
	 * Ajouter une commande
	 * @param parent fenêtre parent (la méthode repaint() sera appelée après l'exécution de la commande)
	 * @param comm
	 */
	public void addCommand(final JComponent parent, final Command comm, final CommandHandler commHandler){
		JButton button = new JButton(comm.getName());
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				comm.execute();
				parent.repaint();
				commHandler.add(comm);
			}
		});
		this.add(button);
	}
}
