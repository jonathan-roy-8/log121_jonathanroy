package recepteur;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

/**
 * Une bouteille de vin
 * @author Patrice Boucher
 */
public class WinBottle extends Bottle{

	public final static float VOLUME = 750;
	public final static int OX_PIXEL = 10;
	public final static int OY_PIXEL = 10;
	public final static int WIDTH_PIXELS = 100;
	public final static int HEIGHT_BODY_PIXELS = 200;
	public final static int HAUTEUR_SHOULDER_PIXELS = 250;
	public final static int HEIGHT_COL_PIXELS = 300 ; 
	public final static int WIDTH_COL_PIXELS = 40 ; 
	public final static int THICKNESS = 10 ; 
	
	public WinBottle(){
		super(VOLUME, null);
	}

	@Override
	public void draw(Graphics2D g2) {
		
		int pos1Col = WIDTH_PIXELS/2-WIDTH_COL_PIXELS/2;
		int pos2Col = WIDTH_PIXELS/2+WIDTH_COL_PIXELS/2;
		
 		g2.setStroke(new BasicStroke( 3 ));
	    g2.setColor(Color.BLACK);
	    g2.rotate(3.1416);
		g2.translate(-WIDTH_PIXELS-OX_PIXEL, -HEIGHT_COL_PIXELS-OY_PIXEL);
		g2.drawLine(0, 0, WIDTH_PIXELS, 0);
		g2.drawLine(WIDTH_PIXELS,0,WIDTH_PIXELS,HEIGHT_BODY_PIXELS);
		g2.drawLine(WIDTH_PIXELS, HEIGHT_BODY_PIXELS, pos2Col, HAUTEUR_SHOULDER_PIXELS);
		g2.drawLine(pos2Col,HAUTEUR_SHOULDER_PIXELS,pos2Col,HEIGHT_COL_PIXELS);
		g2.drawLine(pos2Col, HEIGHT_COL_PIXELS, pos1Col, HEIGHT_COL_PIXELS);
		g2.drawLine(pos1Col, HEIGHT_COL_PIXELS, pos1Col, HAUTEUR_SHOULDER_PIXELS);
		g2.drawLine(pos1Col, HAUTEUR_SHOULDER_PIXELS, 0, HEIGHT_BODY_PIXELS);
		g2.drawLine(0,HEIGHT_BODY_PIXELS,0,0);
		
		if(this.liquid!=null){
		  int hauteur = (int) ((int) this.liquid.getVolume()*(HEIGHT_BODY_PIXELS-THICKNESS)/VOLUME);
		  g2.setColor(super.getLiquide().getCouleur());
		  g2.fillRect(THICKNESS, THICKNESS, WIDTH_PIXELS-2*THICKNESS, hauteur);
		}
	}
	
	@Override
	public Dimension getPreferredSize(){
		int largeur = OX_PIXEL*2 + WIDTH_PIXELS ;//(int) ((int )((double) LARGEUR_PIXELS)*1.2);
		int hauteur = (int) (OY_PIXEL*2 + HEIGHT_COL_PIXELS*1.2);//(int) ((int )((double) HAUTEUR_COL_PIXELS)*1.2);
		return new Dimension(largeur, hauteur);
	}

}
