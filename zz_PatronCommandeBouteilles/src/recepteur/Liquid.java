
package recepteur;
import java.awt.Color;


/**
 * Classe qui représente un liquide
 * @author Patrice Boucher
 */
public class Liquid {

	private float volume=0;
	private Color color;
	
	/**
	 * Constructeur
	 * @param volumne
	 * @precondition: volume >= 0
	 */
	public Liquid(float volume, Color color){
		this.volume = volume;
		this.color = color;
	}
	
	public float  getVolume(){return volume;}
	public Color  getCouleur(){return color;}
	
	/**
	 * Mélange un liquide à ce liquide
	 * @param liquide2
	 * @precondition  liquide2!=null (cette précondition pourrait être considérée comme implicite)
	 * @precondition  this.getVolume()+liquide2.getVolume()>0
	 * @postcondition this.getVolume() >= liquide2.getVolume()
	 */
	public void add(Liquid liquide2){
		
		float volumeTotal = this.volume + liquide2.getVolume();
		
		// La couleur résulte du mélange
		float propThis = this.volume/volumeTotal;
		float propLiq2 = liquide2.getVolume()/volumeTotal;
		int red = (int) (color.getRed()*propThis   + liquide2.getCouleur().getRed()*propLiq2); 
		int blue = (int) (color.getBlue()*propThis + liquide2.getCouleur().getBlue()*propLiq2); 
		int green = (int) (color.getGreen()*propThis + liquide2.getCouleur().getGreen()*propLiq2); 
		int alpha = (int) (color.getAlpha()*propThis + liquide2.getCouleur().getAlpha()*propLiq2); 
		color = new Color(red,green,blue,alpha);
		this.volume = volumeTotal;
	}
	
	/**
	 * Retire un volume de ce liquide et retourne le liquide retiré
	 * @param volume
	 * @precondition:  volume >= 0 , volume >= this.getVolume()
	 * @postcondition: Volume du liquide retourné = volume
	 * @return
	 */
	public Liquid retirerLiquide(float volume){
		Liquid liquide = new Liquid(volume, color);
		this.volume -= volume;
		return liquide;
	}
}
