 
package recepteur;

import java.awt.Dimension;
import java.awt.Graphics2D;

 

/**
 * Une bouteille
 * @author Patrice Boucher
 */
public abstract class Bottle{

	protected Liquid liquid;
	private float volume; //Le volume de la bouteille (et non du liquide)
	
	/**
	 * Constructeur
	 * @param volume 
	 * @param liquide 
	 * @precondition volume > 0
	 */
	public Bottle(float volume, Liquid liquide){
		this.volume = volume;
		this.liquid = liquide;
	}
	
	/**
	 * @return liquide de la bouteille
	 */
	public Liquid getLiquide(){
		return liquid;
	}
	
	/**
	 * Ajoute du liquide dans la bouteille
	 * @param liquide
	 * @precondition  this.getVolumeLiquide() + liquide.getVolume() < this.getVolume()
	 * @postcondition this.getVolumeLiquide() >= liquide.getVolume() 
	 *                this.getLiquide()!=null
	 */
	public void addLiquid(Liquid liquide){
		if(this.liquid==null)
		   this.liquid = liquide;
		else
		   this.liquid.add(liquide);
	}
	
	/**
	 * Retire du liquide de la bouteille
	 * @return liquide retiré
	 * @precondition   volume >= 0, 
	 * @precondition   this.getLiquide()!=null
	 * @precondition   this.getVolumeLiquide()  >= volume 
	 * @postcondition  [liquide retiré].getVolume() = volume 
	 */
	public Liquid removeLiquide(float volume){
		return liquid.retirerLiquide(volume);
	}

	/**
	 * @return volume de la bouteille
	 * @postcondition volume > 0
	 */
	public float getVolume(){return volume;}
	
 
	/**
	 * @return the volume of the liquide
	 * @postcondition volume>=0
	 */
	public float getVolumeLiquid(){
		if(liquid==null)
			return 0;
		else
			return liquid.getVolume();
	}
	
	/**
	 * Dessine la bouteille dans un Graphics2D
	 * @param g2d un Graphics2D
	 */
	public abstract void draw(Graphics2D g2d);
	
	/**
	 * @return dimension préférée du graphique dessiné dans la fonction "draw(Graphics2D g2d)"
	 */
	public abstract Dimension getPreferredSize();
 }
