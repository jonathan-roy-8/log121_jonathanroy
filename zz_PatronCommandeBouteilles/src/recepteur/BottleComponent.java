package recepteur;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;


/**
 * Un JComponent d'une bouteille : ceci est un adapteur
 * @author Patrice Boucher
 */
public class BottleComponent extends JComponent {
	
	private static final long serialVersionUID = -7034861723106372357L;
	private Bottle bottle;
	
	/**
	 * Constructeur
	 * @param bottle
	 */
	public BottleComponent(Bottle bottle){
		this.bottle = bottle;
	}

	@Override
	public void paintComponent(Graphics g){
		Graphics2D g2d = (Graphics2D) g;
		bottle.draw(g2d);
	}
	
	@Override
	public Dimension getPreferredSize(){
		return bottle.getPreferredSize();
	}
}
