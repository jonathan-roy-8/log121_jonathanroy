package commands;

import javax.swing.JOptionPane;

import recepteur.Bottle;

/**
 * Une commande de transfert d'une bouteille à une autre
 * @author Patrice Boucher
 */
public class Transfer extends Command{

	private Bottle bottleSource; // Bouteille source
	private Bottle bottleDest;   // bouteille destinataire
	private float  volume;
	
	/**
	 * Constructeur 
	 * @param bottleSource source 
	 * @param bottleDest   destination
	 * @param volume       volume de liquide transféré
	 * @param commName     nom du transfère
	 */
	public Transfer(Bottle bottleSource, Bottle bottleDest, float volume, String commName){
		super(commName);
		this.bottleSource = bottleSource;
		this.bottleDest = bottleDest;
		this.volume     = volume;
	}
	
	@Override
	public void execute() {
		if(bottleDest.getVolumeLiquid()+volume > bottleSource.getVolume())
			JOptionPane.showMessageDialog(null, "Impossible d'ajouter ce volume à la bouteille 1: elle va débordée");
		else if(bottleSource.getVolumeLiquid() < volume)
			JOptionPane.showMessageDialog(null, "Impossible de retirer un volume inexistant");
		else
			bottleDest.addLiquid(bottleSource.removeLiquide(volume));
			}

	@Override
	public void undo() {
		Transfer transfer = new Transfer(bottleDest,bottleSource,volume,"undo("+commName+")");
		transfer.execute();
	}
}
