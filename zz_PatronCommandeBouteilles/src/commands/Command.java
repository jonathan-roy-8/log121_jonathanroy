
package commands;

/**
 * Une interface de commande 
 * @author Patrice Boucher
 *
 */
public abstract class Command{
	protected String commName;
	
	public Command(String name){
		this.commName = name; 
	}

	/**
	 * Exécute la commande
	 */
	public abstract void   execute();
	   
	/**
	 * Effectue une opération pour défaire la commande
	 */
	public abstract void   undo();
     
     /**
      * 
      * @return nom de la commande
      */
    public  String getName(){
    	return commName;
    }
      
 
}
