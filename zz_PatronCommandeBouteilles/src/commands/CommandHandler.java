package commands;

import java.util.Stack;

/**
 * Emmagasine des commandes pour des opérations de undo-redo
 * @author Patrice Boucher
 *
 */
public class CommandHandler{

	private Stack<Command>  commands = new Stack<Command>();
	private Stack<Command>  commandsForRedo = new Stack<Command>();
	
	/**
	 * Ajoute une commande
	 * @param command
	 */
	public void add(Command command){
		commands.add(command);
		commandsForRedo = new Stack<Command>();
	}
	
	/**
	 * Effectue une opération de undo
	 */
	public void undo() {
		if(!commands.isEmpty()){
		   Command last = commands.pop();
		   last.undo();
		   commandsForRedo.add(last);
		}
	}
	
	/**
	 * Effectue une opération de redo
	 */
	public void redo(){
		if(!commandsForRedo.isEmpty()){
			Command last = commandsForRedo.pop();
			commands.add(last);
			last.execute();
		}
	}
}
