import invoker.ComponentCommander;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import commands.CommandHandler;
import commands.Transfer;

import recepteur.BottleComponent;
import recepteur.WinBottle;
import recepteur.Liquid;


/**
 * Un panneau ayant trois bouteilles et des commandes permettant
 * de transférer les contenus
 * 
 * @author Patrice Boucher
 *
 */
public class BottleFrame {

	public final static int OPACITE_VIN = 100;
	public final static int TRANSFERT = 100; //mL
	
	/**
	 * Constructeur
	 */
	public BottleFrame(){
		JFrame frame = new JFrame();
		final JPanel panel = new JPanel();
		frame.add(panel);
		panel.setLayout(new BorderLayout());
		
		WinBottle bout1 = new WinBottle();
        bout1.addLiquid(new Liquid(WinBottle.VOLUME*(float) 0.75,new Color(Color.RED.getRed(), Color.RED.getGreen(),Color.RED.getBlue(),OPACITE_VIN)));
        
        final WinBottle bout2 = new WinBottle();
        bout2.addLiquid(new Liquid(WinBottle.VOLUME*(float) 0.75,new Color(Color.BLUE.getRed(), Color.BLUE.getGreen(),Color.BLUE.getBlue(), OPACITE_VIN)));
        final WinBottle bout3 = new WinBottle();
        final CommandHandler commHandler = new CommandHandler();
        
        ComponentCommander ccbout1 = new ComponentCommander(new BottleComponent(bout1));
        ccbout1.addCommand(panel,new Transfer(bout1,bout2,TRANSFERT,"1->2"),commHandler);
        ccbout1.addCommand(panel,new Transfer(bout1,bout3,TRANSFERT,"1->3"),commHandler);
        
        ComponentCommander ccbout2 = new ComponentCommander(new BottleComponent(bout2));
        ccbout2.addCommand(panel, new Transfer(bout2,bout1,TRANSFERT,"2->1"), commHandler);
        ccbout2.addCommand(panel, new Transfer(bout2,bout3,TRANSFERT,"2->3"), commHandler);
        
        ComponentCommander ccbout3 = new ComponentCommander(new BottleComponent(bout3));
        ccbout3.addCommand(panel, new Transfer(bout3,bout1,TRANSFERT,"3->1"), commHandler);
        ccbout3.addCommand(panel, new Transfer(bout3,bout2,TRANSFERT,"3->2"), commHandler);
        
        JPanel undoRedoPan = new JPanel();
        undoRedoPan.setLayout(new BoxLayout(undoRedoPan,BoxLayout.X_AXIS));
        JButton undo = new JButton("Undo");
        undo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				commHandler.undo();
				panel.repaint();
			}    	
        });
        undoRedoPan.add(undo);
        JButton redo = new JButton("Redo");
        redo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				commHandler.redo();
				panel.repaint();
			}    	
        });
        undoRedoPan.add(redo);
        
        panel.setLayout(new BorderLayout());
        panel.add(ccbout1,BorderLayout.WEST);
        panel.add(ccbout2,BorderLayout.CENTER);
        panel.add(ccbout3,BorderLayout.EAST);
        panel.add(undoRedoPan,BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BottleFrame bottle = new BottleFrame();
	}

}
