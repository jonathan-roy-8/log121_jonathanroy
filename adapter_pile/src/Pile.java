/*
   auteur: Sébastien Adam

*/

import java.util.*;

public class Pile {

    private ArrayList pile = new ArrayList();

    public boolean estVide( ) { return pile.isEmpty(); }

    public Object sommet( ) { return pile.get(pile.size()-1); }

    public Object depile( ) { return pile.remove(pile.size()-1); }

    public void empile(Object v) { pile.add(v); }

    public String toString( ) { return pile.toString(); }

    public Iterator iterator( ) { return pile.iterator( ); }
}