/*
   auteur: Sébastien Adam

*/

public class TestAdapter {

	public static void main(String[] args) {

		Pile p = new Pile();

		p.empile("Item A");
		p.empile("Item B");
		p.empile("Item C");
		p.empile("Item D");

		System.out.println("Les items sont: ");
		
		while (!p.estVide()) {
			System.out.println(p.depile());
		}
	}

}
