/*
   auteur: Sébastien Adam

   source: http://www.psrg.cs.usyd.edu.au/~comp5028/s2_2004/lectures/Design_patterns_II2ups.pdf 

*/

public abstract class Participant {

    private IChatRoom chatRoom;

    private String name;

    public Participant(String name) { this.name = name; }

    public String getName() { return name; }

    public IChatRoom getChatRoom() { return chatRoom; }

    public void setChatRoom(IChatRoom c) { chatRoom = c; }

    public void send(String to, String msg) { chatRoom.send(name, to, msg); }

    public abstract void receive(String from, String msg);
}
