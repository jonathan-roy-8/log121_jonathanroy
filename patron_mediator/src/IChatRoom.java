/*
   auteur: Sébastien Adam

   source: http://www.psrg.cs.usyd.edu.au/~comp5028/s2_2004/lectures/Design_patterns_II2ups.pdf 

*/

public interface IChatRoom {
    public void register(Participant p);
    public void send(String from, String to, String msg);
}
