/*
   auteur: Sébastien Adam

   source: http://www.psrg.cs.usyd.edu.au/~comp5028/s2_2004/lectures/Design_patterns_II2ups.pdf 

*/

public class Beatle extends Participant {
    
    public Beatle(String name) { super(name); }

    public void receive(String from, String msg)
    {
      System.out.println("** To a Beatle **");
      System.out.println("From " + from + ": " + msg); 
    }
}
