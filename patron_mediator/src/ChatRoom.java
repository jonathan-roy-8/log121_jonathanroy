/*
   auteur: Sébastien Adam

   source: http://www.psrg.cs.usyd.edu.au/~comp5028/s2_2004/lectures/Design_patterns_II2ups.pdf 

*/

import java.util.Hashtable;

public class ChatRoom implements IChatRoom
{
    private Hashtable participants = new Hashtable();

    public void register(Participant p)
    {
      if (participants.get(p.getName()) == null)
      {
        participants.put(p.getName(), p);
      }

      p.setChatRoom(this);
    }

    public void send(String from, String to, String message)
    {
      Participant p = (Participant) participants.get(to);
      if (p != null)
      {
        p.receive(from, message);
      }
    }
}
