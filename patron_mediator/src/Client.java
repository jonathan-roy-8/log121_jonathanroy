/*
   auteur: Sébastien Adam

   source: http://www.psrg.cs.usyd.edu.au/~comp5028/s2_2004/lectures/Design_patterns_II2ups.pdf 

*/

public class Client {

    public static void main(String[] args)
    {
      IChatRoom chatRoom = new ChatRoom();
 
      Participant George = new Beatle("George");
      Participant Paul = new Beatle("Paul");
      Participant Ringo = new Beatle("Ringo");
      Participant John = new Beatle("John") ;
      Participant Yoko = new Nobody("Yoko");

      chatRoom.register(George);
      chatRoom.register(Paul);
      chatRoom.register(Ringo);
      chatRoom.register(John);
      chatRoom.register(Yoko);

      Yoko.send ("John", "Hi John!");
      Paul.send ("Ringo", "All you need is love");
      Ringo.send("George", "My sweet Lord");
      Paul.send ("John", "Can't buy me love");
      John.send ("Yoko", "My sweet love") ;
    }
}
