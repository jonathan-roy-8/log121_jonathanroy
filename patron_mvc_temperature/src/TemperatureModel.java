/*
   auteur: S�bastien Adam

   source: http://csis.pace.edu/~bergin/mvc/mvcgui.html

*/

import java.util.Observable;

public class TemperatureModel extends Observable {
	private double temperatureF = 32.0;

	public double getC() {
		return (temperatureF - 32.0) * 5.0 / 9.0;
	}

	public double getF() {
		return temperatureF;
	}

	public void setC(double tempC) {
		temperatureF = tempC * 9.0 / 5.0 + 32.0;
		setChanged();
		System.out.println("TemperatureModel notify observers because Celcius temperature change in model");
		notifyObservers();
	}

	public void setF(double tempF) {
		temperatureF = tempF;
		setChanged();
		System.out.println("TemperatureModel notify observers because Fahrenheit temperature change in model");
		notifyObservers();
	}
}
