/*
   auteur: S�bastien Adam

   source: http://csis.pace.edu/~bergin/mvc/mvcgui.html

*/

public class MVCTempConvert 
{	public static void main(String args[]) 
	{	
		System.out.println("Create a TemperatureModel");
		TemperatureModel temperature = new TemperatureModel();
		
		new FarenheitGUI(temperature, 100, 100);
		new CelsiusGUI(temperature,100, 250);
		new SliderGUI(temperature,100, 400);
	}
}
