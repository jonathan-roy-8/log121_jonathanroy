/*
   auteur: S�bastien Adam

   source: http://csis.pace.edu/~bergin/mvc/mvcgui.html

*/

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

class CelsiusGUI extends TemperatureGUI {
	class DisplayListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			double value = getDisplay();
			model().setC(value);
		}
	}

	class DownListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			model().setC(model().getC() - 1.0);
		}
	}

	class UpListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			model().setC(model().getC() + 1.0);
		}
	}

	public CelsiusGUI(TemperatureModel model, int h, int v) {
		super("Celsius Temperature", model, h, v);
		setDisplay("" + model.getC());
		addUpListener(new UpListener());
		addDownListener(new DownListener());
		addDisplayListener(new DisplayListener());
	}

	public void update(Observable t, Object o) { // Called from the Model
		System.out.println("TemperatureGUI called from the model");
		setDisplay("" + model().getC());
	}
}
