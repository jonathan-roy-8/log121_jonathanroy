/*
   auteur: S�bastien Adam

   source: http://csis.pace.edu/~bergin/mvc/mvcgui.html

*/

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

class FarenheitGUI extends TemperatureGUI {
	class DisplayListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			double value = getDisplay();
			model().setF(value);
		}
	}

	class DownListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			model().setF(model().getF() - 1.0);
		}
	}

	class UpListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			model().setF(model().getF() + 1.0);
		}
	}

	public FarenheitGUI(TemperatureModel model, int h, int v) {
		super("Farenheit Temperature", model, h, v);
		setDisplay("" + model.getF());
		addUpListener(new UpListener());
		addDownListener(new DownListener());
		addDisplayListener(new DisplayListener());
	}

	public void update(Observable t, Object o) { // Called from the Model
		System.out.println("FarenheightGUI called from the model");
		setDisplay("" + model().getF());
	}
}
