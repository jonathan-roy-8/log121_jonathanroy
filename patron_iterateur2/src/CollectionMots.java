
/**
 * Une collection de mots: pour illustrer le fonctionnement d'un itérateur
 * @author Patrice Boucher
 *
 */
public class CollectionMots {

	private String[] mots;
	
	/**
	 * Constructeur
	 * @param mots tableau de mots
	 */
	public CollectionMots(String[] mots){
		this.mots = mots;
	}
	
	/**
	 * Retourne un mot de la collection
	 * @param index index du mot
	 * @return le mot à l'index spécifié
	 */
	public String get(int index){
		return mots[index];
	}
	
	/**
	 * Efface le mot à un index
	 * @param index 
	 */
	public void remove(int index){
		String[] newMots = new String[mots.length-1];
		for(int i=0, z=0; i<mots.length;++i){
			if(i!=index){
				newMots[z++] = mots[i];
			}
		}
		mots = newMots;
	}
	
	/**
	 * @return nombre de mots
	 */
	public int getLength(){
		return mots.length;
	}
	
	/**
	 * @return un itérateur sur la collection
	 */
	public Iterator<String> iterator(){
		return new IteratorCollectionMots(this);
	}
}
