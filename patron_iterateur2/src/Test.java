
public class Test {
	
	public static void main(String[] args){
		
		String[] mots = {"Beure", "Lait", "Céréales", "Crevettes", "Laitue"};
		CollectionMots collection = new CollectionMots(mots);
		
		Iterator<String> it = collection.iterator();
		
		System.out.println("***Affichage de la collection de mots");
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
		it=collection.iterator();
		System.out.println("***Retrait des Crevettes"); 
		while(it.hasNext()){
			if(it.next().compareTo("Crevettes")==0){
				it.remove();
			}
		}
		
		it=collection.iterator();
		System.out.println("***Affichage de la liste modifiée");
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
	}

}
