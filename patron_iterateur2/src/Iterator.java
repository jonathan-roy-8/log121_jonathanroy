
/** 
 * Interface Iterator
 * Sert au polymorphisme: 
 *   n'importe quelle collection pourrait parcourue, de la même façon, via cet Iterator
 */
public interface Iterator<E> {
	
   /**
    * @return s'il y a d'autres éléments
    */
   boolean hasNext();
   /**
    * Réalise une itération: retourne le prochain élément
    * @return prochain élément
    */
   E       next();
   /**
    * Efface le dernier élément retourné
    */
   void    remove();
}
