
/**
 * Un itérateur sur CollectionMots
 *
 */
public class IteratorCollectionMots implements Iterator<String> {

	private CollectionMots cmots;
	private int index;
	
	/**
	 * Constructeur
	 * @param cmots collection de mots
	 */
	public IteratorCollectionMots(CollectionMots cmots){
		this.cmots = cmots;
		index = 0;
	}
	
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return index < cmots.getLength();
	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
		return cmots.get(index++);
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		index--;
		cmots.remove(index);
	}
}
