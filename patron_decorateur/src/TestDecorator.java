/*
   auteur: S�bastien Adam

   source: http://csis.pace.edu/~bergin/patterns/strategydecorator.html

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class TestDecorator {
	public static void printWhen(String filename, CheckStrategy which)
			throws IOException {
		BufferedReader infile = new BufferedReader(new FileReader(filename));
		String buffer = null;
		while ((buffer = infile.readLine()) != null) {
			StringTokenizer words = new StringTokenizer(buffer);
			while (words.hasMoreTokens()) {
				String word = words.nextToken();
				if (which.check(word))
					System.out.println(word);
			}
		}
	}
	public static void main(String[] args) {
		try {
			System.out.println("Enter file name (test.txt)");
			Scanner clavier = new Scanner(System.in);
			String nomFic = clavier.next();
			CounterDecorator counter = new CounterDecorator (new Palindrome());
			printWhen(nomFic, counter);
			System.out.println("" + counter.count());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
