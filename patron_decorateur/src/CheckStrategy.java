/*
   auteur: Sébastien Adam

   source: http://csis.pace.edu/~bergin/patterns/strategydecorator.html

*/

public interface CheckStrategy {
	public boolean check(String s);
}
