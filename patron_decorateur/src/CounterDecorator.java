/*
   auteur: Sébastien Adam

   source: http://csis.pace.edu/~bergin/patterns/strategydecorator.html

*/

public class CounterDecorator implements CheckStrategy {
	private CheckStrategy checker;

	private int count = 0;

	public CounterDecorator(CheckStrategy check) {
		checker = check;
	}

	public boolean check(String s) {
		boolean result = checker.check(s);
		if (result)
			count++;
		return result;
	}

	public int count() {
		return count;
	}

	public void reset() {
		count = 0;
	}
}
