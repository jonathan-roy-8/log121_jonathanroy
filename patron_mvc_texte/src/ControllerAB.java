
/**
 * LOG121: Conception orientée objet
 * A simple example of the Model/View/Controller architecture
 * ViewAController implements an AbstractController and manages evens 
 * of the ViewAObserver
 * 
 * @author Patrice Boucher
 * @date 2012/10/17
 */

import  java.awt.event.*;

public class ControllerAB implements ActionListener{

	public ControllerAB(TextModel aModel)
	{
		model = aModel;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		updateModel(event.getActionCommand());
	}
	
	/**
	 * Update the model
	 */
	private void updateModel(String content)
	{
		model.setState(content);
	}
	private TextModel model;

}
