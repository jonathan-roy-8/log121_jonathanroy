/**
 * Breuvage ayant un extra chocolat
 * @author Patrice Boucher
 *
 */
public class BreuvageExtraChocolat extends BreuvageExtra{

	private final float extraChocolat = (float)1.00; 
	
	/**
	 * Constructeur
	 * @param breuvage breuvage de base
	 */
	public BreuvageExtraChocolat(Breuvage breuvage) {
		super(breuvage);
	}
	
	@Override
	public float getCost() {
		return super.breuvage.getCost() + extraChocolat;
	}
	@Override
	public String getDescription() {
		return super.breuvage.getDescription()+" + chocolat("+extraChocolat+") ";
	}
}
