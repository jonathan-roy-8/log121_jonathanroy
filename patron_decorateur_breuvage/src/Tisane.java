/**
 * Une tisane
 * @author Patrice Boucher
 */
public class Tisane extends Breuvage{
	
	private final float cost= (float) 1.15;
	
	@Override
	public float getCost() {
		return cost;
	}
	
	@Override
	public String getDescription() {
		return "Tisane exotique ";
	}
}
