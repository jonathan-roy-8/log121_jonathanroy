/**
 * Un client qui test les breuvages
 * @author Patrice Boucher
 */
public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Cafe cafe = new Cafe();
		System.out.println(cafe.getDescription()+"-> "+cafe.getCost()+" $");
		
		BreuvageExtraChocolat cafeChoco = new BreuvageExtraChocolat(cafe); 
		System.out.println(cafeChoco.getDescription()+"-> "+cafeChoco.getCost()+" $");
		
		BreuvageExtraNoix cafeChocoNoix = new BreuvageExtraNoix(cafeChoco); 
		System.out.println(cafeChocoNoix.getDescription()+"-> "+cafeChocoNoix.getCost()+" $");
		
		BreuvageExtraCreme cafeChocoNoixCreme = new BreuvageExtraCreme(cafeChocoNoix); 
		System.out.println(cafeChocoNoixCreme.getDescription()+"-> "+cafeChocoNoixCreme.getCost()+" $");
		

		Tisane tisane = new Tisane();
		System.out.println(tisane.getDescription()+"-> "+tisane.getCost()+" $");
		
		BreuvageExtraCreme tisaneCreme = new BreuvageExtraCreme(tisane); 
		System.out.println(tisaneCreme.getDescription()+"-> "+tisaneCreme.getCost()+" $");
		
		BreuvageExtraLait tisaneLait = new BreuvageExtraLait(tisane); 
		System.out.println(tisaneLait.getDescription()+"-> "+tisaneLait.getCost()+" $");
		
	}
}
