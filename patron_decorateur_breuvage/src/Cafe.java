/**
 * Un café
 * @author Patrice Boucher
 */
public class Cafe extends Breuvage{

	private final float cost= (float) 1.50;
	
	@Override
	public float getCost() {
		return cost;
	}
	
	@Override
	public String getDescription() {
		return "Café filtre ";
	}
}
