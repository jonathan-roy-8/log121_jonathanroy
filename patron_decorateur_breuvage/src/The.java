/**
 * Un thé
 * @author Patrice Boucher
 */
public class The extends Breuvage{

	private final float cost= (float) 1.10;
	
	@Override
	public float getCost() {
		return cost;
	}

	@Override
	public String getDescription() {
		return "Thé chinois  ";
	}
}
