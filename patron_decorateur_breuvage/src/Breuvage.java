/**
 * Un breuvage
 *
 */
public abstract class Breuvage {
	
	/**
	 * 
	 * @return coût du breuvage
	 */
	public abstract float getCost();
	
	/**
	 * 
	 * @return description du breuvage
	 */
	public abstract String getDescription();
}
