 /**
 * Breuvage ayant un extra crème
 * @author Patrice Boucher
 *
 */
public class BreuvageExtraCreme  extends BreuvageExtra{
	private final float extraCreme = (float) 0.50; 
	
	/**
	 * Constructeur
	 * @param breuvage de base
	 */
	public BreuvageExtraCreme(Breuvage breuvage) {
		super(breuvage);
	}
	@Override
	public float getCost() {
		return super.breuvage.getCost() + extraCreme;
	}
	@Override
	public String getDescription() {
		return super.breuvage.getDescription()+" + crème("+extraCreme+") ";
	}
}
