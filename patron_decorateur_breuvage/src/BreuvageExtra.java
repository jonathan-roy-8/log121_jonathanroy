/**
 * Breuvage ayant un extra
 * @author Patrice Boucher
 */
public abstract class BreuvageExtra extends Breuvage{

	protected Breuvage breuvage;
	
	/**
	 * Constructeur
	 * @param breuvage breuvage de base (sans l'extra)
	 */
	public BreuvageExtra(Breuvage breuvage){
		this.breuvage = breuvage;
	}
	
	@Override
	public abstract float getCost();

}
