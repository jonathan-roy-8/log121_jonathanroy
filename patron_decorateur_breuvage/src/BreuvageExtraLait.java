 /**
 * Breuvage ayant un extra lait
 * @author Patrice Boucher
 *
 */
public class BreuvageExtraLait  extends BreuvageExtra{
	private final float extraLait = (float) 0.10; 
	
	/**
	 * Constructeur
	 * @param breuvage de base
	 */
	public BreuvageExtraLait(Breuvage breuvage) {
		super(breuvage);
	}
	@Override
	public float getCost() {
		return super.breuvage.getCost() + extraLait;
	}
	@Override
	public String getDescription() {
		return super.breuvage.getDescription()+" + lait("+extraLait+") ";
	}
}
