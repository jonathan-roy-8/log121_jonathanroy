 /**
 * Breuvage ayant un extra noix
 * @author Patrice Boucher
 *
 */
public class BreuvageExtraNoix  extends BreuvageExtra{
	
	private final float extraNoix = (float) 0.25; 
	
	/**
	 * Constructeur
	 * @param breuvage de base
	 */
	public BreuvageExtraNoix(Breuvage breuvage) {
		super(breuvage);
	}
	@Override
	public float getCost() {
		return super.breuvage.getCost() + extraNoix;
	}
	@Override
	public String getDescription() {
		return super.breuvage.getDescription()+" + noix("+extraNoix+") ";
	}
}
