/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

import java.util.Observable;
import java.util.Observer;

class Investor implements Observer {
	private String name;

	private IBM stock;

	public Investor(String name) {
		this.name = name;
	}

	public IBM getStock() {
		return stock;
	}

	public void setStock(IBM value) {
		stock = value;
	}

	public void update(Observable arg0, Object arg1) {
		setStock(((IBM) arg0));
		System.out.println("Notified " + name + " of " + getStock().getSymbol()
				+ "'s " + "change to " + getStock().getPrice());
	}

}
