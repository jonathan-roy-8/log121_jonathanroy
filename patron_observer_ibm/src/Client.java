/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class Client {
	public static void main(String[] args) {
		IBM ibm = new IBM("IBM", 120.00);
		ibm.addObserver(new Investor("Sorros"));
		ibm.addObserver(new Investor("Berkshire"));

		ibm.setPrice(120.10);
		ibm.setPrice(121.00);
		ibm.setPrice(120.50);
		ibm.setPrice(120.75);
	}
}
