/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

import java.util.Observable;

class IBM extends Observable {

	private double price;

	private String symbol;

	public IBM(String symbol, double price) {
		this.symbol = symbol;
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setPrice(double value) {
		price = value;
		setChanged();
		notifyObservers();
	}

	public void setSymbol(String value) {
		symbol = value;
		setChanged();
		notifyObservers();
	}
}
