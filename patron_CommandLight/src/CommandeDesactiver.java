/**
 * Exemple du Patron Commande tiré de Wikipédia
 * http://fr.wikipedia.org/wiki/Commande_%28patron_de_conception%29
 * 
 * Ajout d'un client graphique pour représenter la lumière et l'interrupteur
 * @author Patrice Boucher
 */

/*the Command for turning off the light - ConcreteCommand #2*/
public class CommandeDesactiver implements Command {
 
   private Lumiere theLight;
 
   public CommandeDesactiver(Lumiere light) {
      this.theLight = light;
   }
 
   public void execute() {
      theLight.desactiver();
   }
 
}
 