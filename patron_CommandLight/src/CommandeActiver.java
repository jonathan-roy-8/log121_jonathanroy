/**
 * Exemple du Patron Commande tiré de Wikipédia
 * http://fr.wikipedia.org/wiki/Commande_%28patron_de_conception%29
 * 
 * Ajout d'un client graphique pour représenter la lumière et l'interrupteur
 * @author Patrice Boucher
 */

/*the Command for turning on the light - ConcreteCommand #1*/
public class CommandeActiver implements Command {
 
   private Lumiere theLight;
 
   public CommandeActiver(Lumiere light) {
      this.theLight = light;
   }
 
   public void execute(){
      theLight.activer();
   }
}