/**
 * Exemple du Patron Commande tiré de Wikipédia
 * http://fr.wikipedia.org/wiki/Commande_%28patron_de_conception%29
 * 
 * Ajout d'un client graphique pour représenter la lumière et l'interrupteur
 * @author Patrice Boucher
 */

import javax.swing.JFrame;

/*The test class or client*/
public class Tester {
 
   public static void main(String[] args){
	   
	   InterfaceClient inter = new InterfaceClient();
   }
 
}