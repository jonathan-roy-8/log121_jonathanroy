import java.awt.BorderLayout;

/**
 * Exemple du Patron Commande tiré de Wikipédia
 * http://fr.wikipedia.org/wiki/Commande_%28patron_de_conception%29
 * 
 * Ajout d'un client graphique pour représenter la lumière et l'interrupteur
 * @author Patrice Boucher
 */

import javax.swing.JFrame;


public class InterfaceClient extends JFrame{
	
	public InterfaceClient(){
		
		this.setLayout(new BorderLayout());
		
		Lumiere lumiere = new Lumiere();
		this.add(lumiere,BorderLayout.CENTER);
		 
		Command cmdActiver    = new CommandeActiver(lumiere);
		Command cmdDesactiver = new CommandeDesactiver(lumiere);
		Interrupteur interrupteur = new Interrupteur(cmdDesactiver, cmdActiver);
		this.add(interrupteur,BorderLayout.WEST);
		  
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
