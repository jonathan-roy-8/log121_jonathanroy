/**
 * Exemple du Patron Commande tiré de Wikipédia
 * http://fr.wikipedia.org/wiki/Commande_%28patron_de_conception%29
 * 
 * Ajout d'un client graphique pour représenter la lumière et l'interrupteur
 * @author Patrice Boucher
 */

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

/*
 * This file is from Wikipedia:
 * http://fr.wikipedia.org/wiki/Commande_%28patron_de_conception%29
 */

/*the Receiver class*/
public class Lumiere extends JComponent{
 
   IconComponent lumiereON;
   IconComponent lumiereOFF;
   IconComponent status;
   public final int WIDTH = 300;
   public final int HEIGHT = 200;
	
   public Lumiere() {
	   lumiereON = new IconComponent("lumiereON.jpg",WIDTH,HEIGHT);
	   lumiereOFF = new IconComponent("lumiereOFF.jpg",WIDTH,HEIGHT);
	   this.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	   this.setVisible(true);
	   status = lumiereOFF;
   }
 
   public void activer() {
      System.out.println("La lumière est allumée");
      status = lumiereON;
      this.repaint();
   }
 
   public void desactiver() {
      System.out.println("La lumière est éteinte");
      status = lumiereOFF;
      this.repaint();
   }
   
   protected void paintComponent(Graphics g){
	  status.paintComponent(g);   
   }
 
}