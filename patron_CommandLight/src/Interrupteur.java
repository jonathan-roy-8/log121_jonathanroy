/**
 * Exemple du Patron Commande tiré de Wikipédia
 * http://fr.wikipedia.org/wiki/Commande_%28patron_de_conception%29
 * 
 * Ajout d'un client graphique pour représenter la lumière et l'interrupteur
 * @author Patrice Boucher
 */

/*the Invoker class*/
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JComponent;
 
public class Interrupteur extends JComponent{
 
	public final int WIDTH = 100;
	public final int HEIGHT = 180;
	
	IconComponent positionBas;
	IconComponent positionHaut;
	IconComponent status;

	Command commandePosBas;
	Command commandePosHaut;
	
 
   public Interrupteur(Command commandePosBas, Command commandePosHaut) {
	   this.commandePosBas = commandePosBas;
	   this.commandePosHaut = commandePosHaut;
	   positionHaut = new IconComponent("interrupteurHaut.jpg",WIDTH,HEIGHT);
	   positionBas  = new IconComponent("interrupteurBas.jpg",WIDTH,HEIGHT);
	   this.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	   this.setVisible(true);
	   status = positionBas;
	   
	   this.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(isPositionHaut())
				{
					setPositionBas();
				}else{
					setPositionHaut();
				}
			}
		 });
   }
   
   public void setPositionHaut(){
	   status = positionHaut;
	   commandePosHaut.execute(); 
	   System.out.println("Interrupteur position haut");
	   this.repaint();
   }
   
   public void setPositionBas(){
	   status = positionBas;
	   commandePosBas.execute();  
	   System.out.println("Interrupteur position bas");
	   this.repaint();
   }
   
   public boolean isPositionHaut(){
	   return status==positionHaut;
   }
   
   protected void paintComponent(Graphics g){
	  status.paintComponent(g);   
   }
}
