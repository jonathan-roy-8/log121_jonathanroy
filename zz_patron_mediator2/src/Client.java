/**
   @author: Sébastien Adam
   source: http://www.psrg.cs.usyd.edu.au/~comp5028/s2_2004/lectures/Design_patterns_II2ups.pdf 
   @version 2 Add javadoc (Patrice Boucher)
*/

/**
 * Classe pour simuler une discussion dans le forum
 *
 */
public class Client {

    public static void main(String[] args)
    {
      IChatRoom chatRoom = new ChatRoom();
 
      Participant George = new Beatle("George Harrison");
      Participant Paul = new Beatle("Paul McCartney");
      Participant Ringo = new Beatle("Ringo Starr");
      Participant John = new Beatle("John Lennon") ;
      Participant Yoko = new Nobody("Yoko");

      chatRoom.register(George);
      chatRoom.register(Paul);
      chatRoom.register(Ringo);
      chatRoom.register(John);
      chatRoom.register(Yoko);

      Yoko.send ("John Lennon", "Hi John!");
      Paul.send ("Ringo Starr", "All you need is love");
      Ringo.send("George Harrison", "My sweet Lord");
      Paul.send ("John Lennon", "Can't buy me love");
      John.send ("Yoko", "My sweet love") ;
    }
}
