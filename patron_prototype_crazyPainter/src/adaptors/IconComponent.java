package adaptors;
 
 
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JComponent;

import fabric.IconFabric;

/**
 * Adapte un Icon en JComponent
 * @author patrice
 */
public class IconComponent extends JComponent{

	private static final long serialVersionUID = -8830927664442804569L;
	private Icon icon = null; 
	
	/**
	 * Constructeur
	 * @param icon le nom de l'image
	 */
	public IconComponent(String name, int width, int height){
		this.icon = IconFabric.createImageIcon(name, width, height);
	}
	
	@Override
	public void paintComponent(Graphics g){
		icon.paintIcon(this, g, 0, 0);
	}
	
	@Override
	public Dimension getPreferredSize(){
		return new Dimension(icon.getIconWidth(), icon.getIconHeight());
	}
}
