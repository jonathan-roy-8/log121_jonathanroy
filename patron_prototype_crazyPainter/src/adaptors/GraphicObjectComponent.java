package adaptors;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import framework.GraphicObject;

/**
 * Adapte un GraphicObject en JComponent
 * @author Patrice Boucher
 */
public class GraphicObjectComponent extends JComponent{

	private static final long serialVersionUID = 3924441296548263768L;
    private GraphicObject graphObject;
    
    public GraphicObjectComponent(GraphicObject graphObject){
    	this.graphObject = graphObject;
    }
	
    @Override
    public void paintComponent(Graphics g){
    	graphObject.paint(g);
    }
	
    @Override
    public Dimension getPreferredSize(){
    	return graphObject.getPreferredSize();
    }
}
