package fabric;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class IconFabric {

	/**
	 * Créer un ImageIcon à partir d'un nom de fichier 
	 * @param fileName nom de fichier
	 * @param width  largeur de l'icon
	 * @param height hauteur de l'icon
	 * @return
	 */
	public static ImageIcon createImageIcon(String fileName, int width, int height){
		Image image;
		Image image2=null;
		try {
			image = ImageIO.read(new File(fileName));
			image2 = image.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ImageIcon(image2); //icon;
	}
}
