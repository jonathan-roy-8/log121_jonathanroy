package framework;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

public class Painting extends GraphicObject{
	private ArrayList<GraphicObject> painting = new ArrayList<GraphicObject>();
    Dimension dimension;
    
    /**
     * Constructeur
     * @param dimension
     */
    public Painting(Dimension dimension){
    	this.dimension = dimension;
    }
    
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, dimension.width, dimension.height);
		for(GraphicObject obj : painting){
			System.out.print("ho");
			obj.paint(g);
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return dimension;
	}
	
	/**
	 * Ajouter un objet graphique
	 * @param graph
	 */
	public void addGraphicObject(GraphicObject graph){
		painting.add(graph);
	}
	
	/**
	 * Ajouter un tableau de GraphicObject
	 * @param graph tableau de GraphicObject
	 */
	public void addGraphicObjects(GraphicObject[] graph){
		for(int i=0; i< graph.length ; ++i){
			addGraphicObject(graph[i]);
		}
	}
	
	/**
	 * 
	 * @return itérateur sur la collection de GraphicObject
	 */
	public Iterator<GraphicObject> iteratorGraphicObjects(){
		return painting.iterator();
	}

	@Override
	public GraphicObject clone() {
		Iterator<GraphicObject> it = painting.iterator();
		Painting painting = new Painting(dimension);
		while(it.hasNext()){
			painting.addGraphicObject(it.next().clone());
		}
		return painting;
	}
}
