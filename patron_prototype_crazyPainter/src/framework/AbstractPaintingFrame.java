
package framework;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToolBar;

import adaptors.GraphicObjectComponent;
import adaptors.GraphicObjectIcon;


public abstract class AbstractPaintingFrame extends JFrame{

	private Painting painting;
	private GraphicObject goToClone = null;
	
	
	public AbstractPaintingFrame(Dimension dimension){
		painting = new Painting(dimension);
		this.setLayout(new BorderLayout());
		
		painting.addGraphicObjects(getGraphObjects());
		
		JToolBar bar = new JToolBar();
		Iterator<GraphicObject> itGO = painting.iteratorGraphicObjects();
		while(itGO.hasNext()){
			final GraphicObject next = itGO.next();
			JButton button = new JButton();
			button.setIcon(new GraphicObjectIcon(next));
			button.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent arg0) {
					goToClone = next;
					System.out.println("Sélection "+goToClone.toString());
				}
			});
			bar.add(button);
		}
		JButton button = new JButton("Désélectionner");
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				goToClone = null;
			}
		});
		bar.add(button);
		
		JComponent goComponent = new GraphicObjectComponent(painting);
		goComponent.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent event) {
//				System.out.println("AddObject");
//				System.out.println(event.getLocationOnScreen	());
				addObject(event.getPoint());
			}
		});
		
		
		this.add(bar,BorderLayout.NORTH);
		this.add(goComponent,BorderLayout.CENTER);
		//this.add(new GraphicObjectComponent(painting), BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected abstract GraphicObject[] getGraphObjects();
	
	
	private void addObject(Point position){
		if(goToClone!=null){
			GraphicObject go = goToClone.clone();
			go.setPosition(position);
			painting.addGraphicObject(go);
			System.out.println("Dessin "+goToClone.toString());
			repaint();
		}
		else
			System.out.println("Dessin null");
	}
	
}
