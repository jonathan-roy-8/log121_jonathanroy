package framework;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Un objet pouvant être affiché à une position d'un graphique
 * @author Patrice Boucher
 */
public abstract class GraphicObject {
    protected Point position = new Point(100,100);
	
    /**
     * Constructeur
     */
	 public GraphicObject(){}
	 
	 /**
	  * Ajuste la position de l'objet 
	  * @param point position
	  */
	 public void setPosition(Point point){
		 System.out.println(point);
		 position = point;
	 }
	 
	 /**
	  * 
	  * @return position
	  */
	 public Point getPosition(){
		 return position;
	 }
	 
	 /**
	  * Fonction qui ajoute l'image du graphique présent à un graphique
	  * @param g graphique
	  */
	 public abstract void paint(Graphics g);
	 
	 /**
	  * 
	  * @return dimension de l'image
	  */
	 public abstract Dimension getPreferredSize();
	 
	 public abstract GraphicObject clone();
}
