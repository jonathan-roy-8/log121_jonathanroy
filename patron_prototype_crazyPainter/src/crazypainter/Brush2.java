package crazypainter;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import adaptors.IconComponent;

import fabric.IconFabric;
import framework.GraphicObject;

public class Brush2 extends GraphicObject{

	private final static Dimension dimension = new Dimension(20,20);
	private final static Icon imBrush = IconFabric.createImageIcon("jpg/Brush2.jpg",dimension.width,dimension.height);
	
	@Override
	public void paint(Graphics g) {
		imBrush.paintIcon(null,g, position.x, position.y);
	}

	@Override
	public Dimension getPreferredSize() {
 		return dimension;
	}

	@Override
	public GraphicObject clone() {
 		return new Brush2();
	}

	@Override
	public String toString(){
		return "Brush2 - "+dimension;
	}
}
