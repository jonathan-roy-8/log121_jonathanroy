package crazypainter;

import java.awt.Dimension;

import framework.AbstractPaintingFrame;
import framework.GraphicObject;

public class CrazyPainterFrame extends AbstractPaintingFrame{

 
	private static final long serialVersionUID = -4495376149726948146L;

	/**
	 * Constructeur
	 * @param dimension
	 */
	public CrazyPainterFrame(Dimension dimension) {
		super(dimension);
	}

	@Override
	protected GraphicObject[] getGraphObjects() {
		GraphicObject[] go = {new Brush1(),new Brush2()};
		return go;
	}

}
