package memento;

  
/**
 * Encapsule et protège l'état d'un objet
 * @author Patrice Boucher
 *
 * @param <T> type de l'objet
 */
public class Memento<T>{
	private T object;

	/**
	 * Constructeur
	 * @param object
	 */
	public   Memento(T object){this.object = object;}
	
	/**
	 * @return objet
	 */
	public T getState(){return object; }
}