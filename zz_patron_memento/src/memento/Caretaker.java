package memento;
import java.util.ArrayList;

/**
 * Permet d'accumuler et d'accéder à des Memento (les éléments ajouter ne peuvent être modifiés par la suite) 
 * @author Patrice Boucher
 *
 * @param <T> type d'objet sauvegardé par le Memento
 */
public class Caretaker<T>{

	private ArrayList<Memento<T>> mementos = new ArrayList<Memento<T>>();

	/**
	 * Ajoute un Memento
	 * @param memento
	 */
    public void addMemento(Memento<T> memento){
    	mementos.add(memento);
    }
    
    /**
     * Retourne un Memento
     * @param index
     * @return memento à l'index spécifié
     */
    public Memento<T> getMemento(int index){
    	return mementos.get(index);
    }
    
    /**
     * @return nombre de memento
     */
    public int size(){
    	return mementos.size();
    }
}
