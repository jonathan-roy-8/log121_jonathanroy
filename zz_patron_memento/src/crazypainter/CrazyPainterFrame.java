package crazypainter;

import java.awt.Dimension;

import framework.PaintingFrame;
import framework.Paintable;

/**
 * Une fenêtre concrète pour dessiner des peintures. 
 * @author patrice
 *
 */
public class CrazyPainterFrame extends PaintingFrame{

	private static final long serialVersionUID = -4495376149726948146L;

	/**
	 * Constructeur
	 * @param dimension
	 */
	public CrazyPainterFrame(Dimension dimension) {
		super(dimension);
	}
	@Override
	protected Paintable[] getPrototypes() {
		Paintable[] paintables = {new Brush1(),new Brush2()};
		return paintables;
	}
}
