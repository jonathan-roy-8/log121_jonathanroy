package crazypainter;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import framework.Paintable;

/**
 * Pinceau de type 1
 * @author Patrice Boucher
 */
public class Brush1 extends Paintable{

	private static final long serialVersionUID = -4784302958247758818L;
	private final static Dimension dimension = new Dimension(10,10);
	
	@Override
	public void paint(Graphics g) {
		for(double d=0 ; d < Math.PI ; d+=0.5){
			int dx =  (int)((double)dimension.width*Math.cos(d)/2);
			int dy =  (int)((double)dimension.height*Math.sin(d)/2);
			g.setColor(Color.BLACK);
			g.drawLine(position.x-dx, position.y-dy, position.x+dx, position.y+dy);
		}
	}
	@Override
	public Dimension getPreferredSize() {
		// TODO Auto-generated method stub
		return dimension;
	}
	@Override
	public Paintable clone() {
		Brush1 brush = new Brush1();
		brush.setPosition(position);
		return brush;
	}
	@Override
	public String toString(){
		return "Brush1 - "+dimension;
	}
}
