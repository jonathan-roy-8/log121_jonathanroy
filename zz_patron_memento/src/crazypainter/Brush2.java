package crazypainter;
//import TestAdapter;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.Icon;

import fabric.IconFabric;
import framework.Paintable;

/**
 * Pinceau de type 2
 * @author Patrice Boucher
 */
public class Brush2 extends Paintable{

	private static final long serialVersionUID = -2295396064278065340L;
	private final static Dimension dimension = new Dimension(20,20);
	static java.net.URL imageURL = Brush2.class.getResource("Brush2.png");
	
	private final static Icon imBrush = IconFabric.createImageIcon(imageURL,dimension.width,dimension.height);
//	private final static Icon imBrush = IconFabric.createImageIcon("Brush2.png",dimension.width,dimension.height);
	
	@Override
	public void paint(Graphics g) {
		imBrush.paintIcon(null,g, position.x, position.y);
	}
	@Override
	public Dimension getPreferredSize() {
 		return dimension;
	}
	@Override
	public Paintable clone() {
		Brush2 brush = new Brush2();
		brush.setPosition(position);
		return brush;
	}
	@Override
	public String toString(){
		return "Brush2 - "+dimension;
	}
}
