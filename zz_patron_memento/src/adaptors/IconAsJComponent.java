package adaptors;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JComponent;
import java.awt.Image;

import fabric.IconFabric;

/**
 * Adapte un Icon en JComponent
 * @author Patrice Boucher
 */
public class IconAsJComponent extends JComponent{

	private static final long serialVersionUID = -8830927664442804569L;
	private Icon icon = null; 
	
	/**
	 * Constructeur
	 * @param icon nom de l'image
	 * @param width largeur de l'image
	 * @param height hauteur de l'image
	 */
	public IconAsJComponent(String name, int width, int height){
		this.icon = IconFabric.createImageIcon(name, width, height);
	}
	@Override
	public void paintComponent(Graphics g){
		icon.paintIcon(this, g, 0, 0);
	}
	@Override
	public Dimension getPreferredSize(){
		return new Dimension(icon.getIconWidth(), icon.getIconHeight());
	}
}
