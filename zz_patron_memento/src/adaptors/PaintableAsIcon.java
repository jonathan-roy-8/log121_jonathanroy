package adaptors;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.Icon;

import framework.Paintable;

/**
 * Adapte un GraphicObject en Icon
 * @author Patrice Boucher
 */
public class PaintableAsIcon implements Icon{
	
	private Paintable graph;
	//private Component parent;

    public	PaintableAsIcon(Paintable graph){
    	this.graph = graph;
    }
	
	@Override
	public int getIconHeight() {
 		return graph.getPreferredSize().height;
	}

	@Override
	public int getIconWidth() {
 		return graph.getPreferredSize().width;
	}

	@Override
	public void paintIcon(Component arg0, Graphics g, int x, int y) {
 		graph.setPosition(new Point(x, y));
 		graph.paint(g);
	}
}
