package fabric;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class IconFabric {

	/**
	 * Créer un ImageIcon à partir d'un nom de fichier 
	 * @param name nom de fichier
	 * @param width  largeur de l'icon
	 * @param height hauteur de l'icon
	 * @return
	 */
	public static ImageIcon createImageIcon(String name, int width, int height){
		Image image;
		Image image2=null;
		try {
//			image = ImageIO.read(new File(imageURL));
			image = new Image(name,100,100);
			image2 = image.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ImageIcon(image2); //icon;
	}
}
