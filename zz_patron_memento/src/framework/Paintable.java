package framework;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JComponent;

/**
 * Un objet pouvant être affiché à une position d'une peinture
 * @author Patrice Boucher
 */
public abstract class Paintable extends JComponent{

	private static final long serialVersionUID = 8827333651677929611L;
	protected Point position = new Point(0,0);
	
    /**
     * Constructeur
     */
	 public Paintable(){}
	 
	 /**
	  * Ajuste la position de l'objet 
	  * @param point position
	  */
	 public void setPosition(Point point){
		 position = point;
	 }
	 
	 /**
	  * @return position
	  */
	 public Point getPosition(){
		 return position;
	 }
	 
	 @Override
	 public abstract void paint(Graphics g);
	 
	 @Override
	 public abstract Dimension getPreferredSize();
	 
	 /**
	  * Retourne un clone l'objet
	  * @return clone
	  */
	 public abstract Paintable clone();
}
