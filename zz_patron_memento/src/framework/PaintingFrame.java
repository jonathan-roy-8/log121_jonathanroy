
package framework;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import memento.Caretaker;
import adaptors.PaintableAsIcon;

/**
 * Classe abstraite pour la fenêtre principale du logiciel de peinture
 * @author Patrice Boucher
 */
public abstract class PaintingFrame extends JFrame{


	private static final long serialVersionUID = -2155600298333251864L;
	private Painting painting;
	private Paintable toClone = null;
	private Caretaker mementos  = new Caretaker();
	final   JToolBar  mementoBar = new JToolBar();
	
	/**
	 * Constructeur
	 * @param dimension dimension de la peinture
	 */
	public PaintingFrame(Dimension dimension){
		painting = new Painting(dimension);
		this.setLayout(new BorderLayout());
		
		// Obtention des prototypes
		Paintable[] prototypes = getPrototypes();
		
		// Construction d'une barre permettant de sélectionner le prototype à dessiner
		JToolBar bar = new JToolBar();
		
		// Construction d'un bouton par prototype
		for(int i=0 ; i< prototypes.length ; ++i){
			JButton button = new JButton();
			final Paintable prototype = prototypes[i];
			// Le bouton affichera le prototype via un Icon
			button.setIcon(new PaintableAsIcon(prototype));
			// Lorsque le bouton est sélectionner, le prototype courant est à cloner
			button.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent arg0) {
					toClone = prototype;
				}
			});
			bar.add(button);
		}
		// Pour qu'aucun prototype ne soit dessiner
		JButton button = new JButton("aucun");
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toClone = null;
			}
		});
		bar.add(button);
		
		// Lorsque l'usager clique sur la peinture, le prototype sélectionné est cloné à la position du clic
		painting.addMouseListener(new MouseAdapter(){
			@Override
			public void mousePressed(MouseEvent event) {
				cloneProtoype(event.getPoint());
			}
		});
		
		// Bouton pour ajouter des memento
		JButton ajoutMemento = new JButton("Memento++");
		ajoutMemento.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				addMemento();
			}
		});
		mementoBar.add(ajoutMemento);
		
		this.add(bar,BorderLayout.NORTH);
		this.add(painting,BorderLayout.CENTER);
		this.add(mementoBar,BorderLayout.SOUTH);
		//this.add(new GraphicObjectComponent(painting), BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Implémenter par les classes concrètes, retourne les prototypes de "Paintable" pouvant être clonés
	 * @return
	 */
	protected abstract Paintable[] getPrototypes();
	
	
	/**
	 * Pour cloner le prototype sélectionné
	 * @param position
	 */
	private void cloneProtoype(Point position){
		if(toClone!=null){
			Paintable prototype = toClone.clone();
			prototype.setPosition(position);
			painting.addPaintable(prototype);
			repaint();
		}
	}
	
	/**
	 * Ajoute un memento et un bouton à la barre de bouton memento
	 */
	private void addMemento(){
		mementos.addMemento(painting.createMemento());
 		final JButton button = new JButton(Integer.toString(mementos.size()));
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = Integer.parseInt(button.getText())-1;
				System.out.print("set memento: "+index);
				painting.setMemento(mementos.getMemento(index));
				repaint();
			}
		});
		mementoBar.add(button);
	}
}
