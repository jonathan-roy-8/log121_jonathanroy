package framework;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Iterator;
import memento.Memento;

/**
 * Peinture
 * @author Patrice Boucher
 */
public class Painting extends Paintable{

	private static final long serialVersionUID = -1112365141068545085L;
	private CollectionPaintables paintables = new CollectionPaintables();
    Dimension dimension;
    
    /**
     * Constructeur
     * @param dimension
     */
    public Painting(Dimension dimension){
    	this.dimension = dimension;
    }
    
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, dimension.width, dimension.height);
		for(Paintable obj : paintables){
			obj.paint(g);
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return dimension;
	}
	
	/**
	 * Ajouter un objet graphique
	 * @param graph
	 */
	public void addPaintable(Paintable graph){
		paintables.add(graph);
	}
	
	/**
	 * 
	 * @return itérateur sur la collection de Paintable
	 */
	public Iterator<Paintable> iterator(){
		return paintables.iterator();
	}
	@Override
	public Paintable clone() {
		Iterator<Paintable> it = paintables.iterator();
		Painting painting = new Painting(dimension);
		while(it.hasNext()){
			painting.addPaintable(it.next().clone());
		}
		return painting;
	}
	
	/**
	 * Crée un Memento à partir de la collection d'objets
	 * @return memento
	 */
    public Memento<CollectionPaintables> createMemento(){ 	
    	return new Memento<CollectionPaintables>(paintables.clone());
    }
    
    /**
     * Réinitialise l'état de Painting à partir d'un Memento
     * @param memento
     */
    public void setMemento(Memento<CollectionPaintables> memento){
    	paintables = ((CollectionPaintables) memento.getState()).clone();
    }
}
