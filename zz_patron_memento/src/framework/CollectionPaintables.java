package framework;

import java.util.ArrayList;

public class CollectionPaintables extends ArrayList<Paintable> implements Cloneable{

	private static final long serialVersionUID = -876380965783092316L;
	
	/**
	 * Clone la collection
	 */
	public CollectionPaintables clone(){
		CollectionPaintables collection = new CollectionPaintables();
		for(Paintable paintable : this)
			collection.add(paintable.clone());
		return collection;
	}
}
