
/**
 * This class represents a financial item
 * @author Patrice Boucher
 *
 */
public abstract class FinancialItem {
	
	private String name;
	
	/**
	 * Constructor
	 * @param name financial item's name 
	 */
	public FinancialItem(String name){
		this.name = name;
	}

	/**
	 * Print the financial item via System.out
	 * @param indentation
	 */
	public abstract void print(int indentation);
	
	/**
	 * 
	 * @return return the element's value
	 */
	public abstract double getValue();
	
	/**
	 * 
	 * @return financial item's name
	 */
	public String getName(){
		return name;
	}
}
