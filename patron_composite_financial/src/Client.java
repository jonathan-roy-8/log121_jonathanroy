 
/**
 * A client that tests the financial classes 
 * @author Patrice Boucher
 *
 */
public class Client {

	/**
	 * Constructor
	 */
	public Client(){
		FinancialFolder dossDepProp = new FinancialFolder("Dépenses propriétés");
		FinancialFolder dossMaison  =  new FinancialFolder("Maison");
		FinancialFolder dossElectricite = new FinancialFolder("Électricité");
		FinancialFolder dossTerrain = new FinancialFolder("Terrain");
		FinancialFolder dossRenovation = new FinancialFolder("Rénovations");
		FinancialFolder dossNotaire = new FinancialFolder("Frais notaires");
		FinancialFolder dossTaxesMunicipales = new FinancialFolder("Taxes municipales");
		FinancialFolder dossAqueduc = new FinancialFolder("Aqueduc");
		FinancialFolder dossToiture = new FinancialFolder("Toiture");
		FinancialFolder dossBalcon = new FinancialFolder("Balcon");
		
		dossDepProp.addFinancialItem(dossMaison);
		dossDepProp.addFinancialItem(dossTerrain);
		dossDepProp.addFinancialItem(dossNotaire);
		dossDepProp.addFinancialItem(dossTaxesMunicipales);
		
		dossMaison.addFinancialItem(dossRenovation);
		dossMaison.addFinancialItem(dossElectricite);
		dossMaison.addFinancialItem(dossAqueduc);
		dossMaison.addFinancialItem(dossTaxesMunicipales);
		
		dossRenovation.addFinancialItem(dossToiture);
		dossRenovation.addFinancialItem(dossBalcon);
		
		Invoice facMaison = new Invoice("achat maison", 325000, new Date(2012,6,14,12,0));
		dossMaison.addFinancialItem(facMaison);
		Invoice facTerrain = new Invoice("achat terrain", 35000, new Date(2012,6,14,12,0));
		dossTerrain.addFinancialItem(facTerrain);
		
		Invoice facToitureCedre = new Invoice("toiture en Bardeau de Cèdre", 2000, new Date(2012,8,26,14,30));
		Invoice facToitureJP = new Invoice("travail de Jean-Pierre Boulay", 500, new Date(2012,8,30,16,00));
		dossToiture.addFinancialItem(facToitureCedre);
		dossToiture.addFinancialItem(facToitureJP);
		
		Invoice facElecJuillet = new Invoice("Électricité juillet",67, new Date(2012,8,01,0,1));
		Invoice facElecAout = new Invoice("Électricité août",82, new Date(2012,9,01,0,1));
		dossElectricite.addFinancialItem(facElecJuillet);
		dossElectricite.addFinancialItem(facElecAout);
		
        Invoice facNotaireEmprunt = new Invoice("Frais d'emprunt hypothécaire", 876, new Date(2012,6,9,12,0));
		Invoice facNotaireTransfert = new Invoice("Frais de transfert de propriété", 750, new Date(2012,6,14,12,0));
        dossNotaire.addFinancialItem(facNotaireEmprunt);
        dossNotaire.addFinancialItem(facNotaireTransfert);
		
		dossDepProp.print(0);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Client client = new Client();
		
	}

}
