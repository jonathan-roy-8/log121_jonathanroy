import java.util.ArrayList;

/**
 * This class represents a folder of financial items
 * @author Patrice Boucher
 *
 */
public class FinancialFolder extends FinancialItem {

	private ArrayList<FinancialItem> items;
	
	/**
	 * Constructor
	 * @param name repertory name
	 */
	public FinancialFolder(String name){
		super(name);
		items = new ArrayList<FinancialItem>();
	}
	
	@Override
	public void print(int indentation) {
		// TODO Auto-generated method stub
		String ind="";
		for(int i=0 ; i< indentation ; ++i)
			ind+=" ";
		System.out.println(ind+super.getName()+"("+getValue()+"$)");
		for(FinancialItem element : items){
			element.print(indentation+3);
		}
	}
	@Override
	public double getValue(){
		double value = 0;
		for(FinancialItem element : items){
			value+= element.getValue();
		}
		return value;
	}
	
	/**
	 * Add a financial item to the folder
	 * @param item
	 */
	public void addFinancialItem(FinancialItem item){
		items.add(item);
	}
	
}
