 
 
/**
 * Une date simple
 * @author patrice
 *
 */
public class Date
{
   /**
      Constructs a Date with a given year, month, and Date
      of the Julian/Gregorian calendar. The Julian calendar
      is used for all Dates before October 15, 1582
      @param aYear a year != 0
      @param aMonth a month between 1 and 12
      @param day a date between 1 and 31
   */
   public Date(int aYear, int aMonth, int day, int hour, int min){
      this.year = aYear;
      this.month = aMonth;
      this.date = day;
      this.hour = hour;
      this.min  = min;
      
   }

   /**
      Returns the year of this Date
      @return the year
   */
   public int getYear(){
      return year;
   }

   /**
      Returns the month of this Date
      @return the month
   */
   public int getMonth(){
      return month;
   }

   /**
      Returns the Date of the month of this Date
      @return the Date of the month
   */
   public int getDate(){
      return date;
   }
   
   public int getHour(){
	   return hour;
   }
   
   public int getMin(){
	   return min;
   }
 
   public String toString(){
	   return year+"/"+month+"/"+date+":"+hour+"h"+min;
   }
   private int year;
   private int month;
   private int date;
   private int hour;
   private int min;
}




