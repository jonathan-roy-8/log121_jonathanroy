 

/**
 * An invoice
 * @author Patrice Boucher
 *
 */
public class Invoice extends FinancialItem{

    private double value;
    private Date date; 
    
    /**
     * Constructor
     * @param name name
     * @param value value
     * @param date date
     */
    public Invoice(String name, double value, Date date){
    	super(name);
    	this.value = value;
    	this.date = date;
    }
    @Override
    public double getValue(){
    	return value;
    }
    
    /**
     * 
     * @return date
     */
    public Date getDate(){
    	return date;
    }
    @Override
    public void print(int indentation){
    	String ind="";
		for(int i=0 ; i< indentation ; ++i)
			ind+=" ";
		System.out.println(ind+getDate()+", "+getName()+" - "+getValue()+"$ - ");
    }
}
