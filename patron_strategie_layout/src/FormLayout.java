import java.awt.*;

/**
   A layout manager that lays out components along a central axis
*/
public class FormLayout implements LayoutManager
{  
	// Calcul la dimension préférée à partir de celle des composants contenus
	@Override
   public Dimension preferredLayoutSize(Container parent)
   {  
      Component[] components = parent.getComponents();
      left = 0;
      right = 0;
      height = 0;
      // On cherche la largeur maximale à gauche et droite de l'axe central
      for (int i = 0; i < components.length; i += 2)
      {
         Component cleft = components[i];
         Component cright = components[i + 1];

         Dimension dleft = cleft.getPreferredSize();
         Dimension dright = cright.getPreferredSize();
         left = Math.max(left, dleft.width);
         right = Math.max(right, dright.width);
         height = height + Math.max(dleft.height,
               dright.height);
      } 
      //On ajoute un espace constant aux largeurs maximales
      return new Dimension(left + GAP + right, height);
   }
   @Override
   public Dimension minimumLayoutSize(Container parent)
   {  
      return preferredLayoutSize(parent);
   }

   @Override
   public void layoutContainer(Container parent)
   {  
      preferredLayoutSize(parent); // Sets left, right

      Component[] components = parent.getComponents();

      Insets insets = parent.getInsets();
      int xcenter = insets.left + left;
      int y = insets.top;

      for (int i = 0; i < components.length; i += 2)
      {
         Component cleft = components[i];
         Component cright = components[i + 1];

         Dimension dleft = cleft.getPreferredSize();
         Dimension dright = cright.getPreferredSize();

         int height = Math.max(dleft.height, dright.height);

         //Ajuste les frontières du composant de gauche
         cleft.setBounds(xcenter - dleft.width, y + (height 
               - dleft.height) / 2, dleft.width, dleft.height);

         //Ajuste les frontières du composant de droit
         cright.setBounds(xcenter + GAP, y + (height 
               - dright.height) / 2, dright.width, dright.height);

         //Met à jour la position verticale pour représenter les composants
         // les uns en dessous des autres.
         y += height;
      }
   }

   @Override
   public void addLayoutComponent(String name, Component comp)
   {}

   @Override
   public void removeLayoutComponent(Component comp)
   {}

   private int left;
   private int right;
   private int height;
   private static final int GAP = 6;
}
