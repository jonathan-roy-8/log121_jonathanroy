/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

import java.util.ArrayList;

public class Invocateur {

	private ArrayList commandes = new ArrayList();

	private int cIndex = 0;

	public void faire(int combien) {
		for (int i = 0; i < combien && cIndex < commandes.size(); i++) {
			Commande c = (Commande) commandes.get(cIndex++);
			c.faireCommande();
			System.out.println("faire: " + c);
		}
	}

	public void defaire(int combien) {
		for (int i = 0; i < combien && cIndex > 0; i++) {
			Commande c = (Commande) commandes.get(--cIndex);
			c.defaireCommande();
			System.out.println("defaire: " + c);
		}
	}

	public void ajouterCommande(Commande commande) {
		commandes.add(commande);
	}

}
