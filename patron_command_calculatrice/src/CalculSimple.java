/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class CalculSimple implements Commande {

	private char operateur;

	private int operande;

	private Calculatrice calcu;

	public CalculSimple(char operateur, int operande, Calculatrice c) {
		this.operateur = operateur;
		this.operande = operande;
		this.calcu = c;
	}

	public void faireCommande() {
		calcu.faireOperation(operateur, operande);
	}

	public void defaireCommande() {
		char undo = ' ';
		switch (operateur) {
		case '+':
			undo = '-';
			break;
		case '-':
			undo = '+';
			break;
		case '*':
			undo = '/';
			break;
		case '/':
			undo = '*';
			break;
		}
		calcu.faireOperation(undo, operande);
	}

	public String toString() {
		return operateur + " " + operande;
	}
}
