/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class Client {

	public static void main(String[] args) {
		Invocateur inv = new Invocateur();
		Calculatrice cal = new Calculatrice();

		inv.ajouterCommande(new CalculSimple('+', 5, cal));

		inv.ajouterCommande(new CalculSimple('*', 2, cal));

		inv.ajouterCommande(new CalculSimple('-', 2, cal));

		inv.ajouterCommande(new CalculSimple('/', 4, cal));

		inv.faire(4);

		inv.defaire(3);

		System.out.println("valeur: " + cal.getValeur());
	}
}
