/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class Calculatrice {
	private int valeur = 0;

	public void faireOperation(char operateur, int operande) {
		switch (operateur) {
		case '+':
			valeur += operande;
			break;
		case '-':
			valeur -= operande;
			break;
		case '*':
			valeur *= operande;
			break;
		case '/':
			valeur /= operande;
			break;
		}
	}

	public int getValeur() {
		return valeur;
	}
}
