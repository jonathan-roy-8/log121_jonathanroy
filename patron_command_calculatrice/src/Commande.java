/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public interface Commande {
	public void faireCommande();

	public void defaireCommande();

	public String toString();
}
