/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class Rapport extends Document {
    public void CreerPages()
    {
      addPage(new IntroductionPage());
      addPage(new DeveloppementPage());
      addPage(new ConclusionPage());
      addPage(new SommairePage());
      addPage(new BibliographiePage());
    }
}
