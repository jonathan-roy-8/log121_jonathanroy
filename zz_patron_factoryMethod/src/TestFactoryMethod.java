/*
   auteur: S�bastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

import java.util.ArrayList;

public class TestFactoryMethod {

	public static void main(String[] args) {

		System.out.println("TestFactoryMethod: main");
		
		System.out.println("Create curriculum");
		Document cur = new Curriculum();
		
		System.out.println("getPages");
        ArrayList curPages = cur.getPages();
        
		System.out.println("Create Rapport");
		Document rap = new Rapport();

//		ArrayList rap = ((Document) rap).getPages();

                // ...
	}
}
