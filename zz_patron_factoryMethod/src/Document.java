/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

import java.util.ArrayList;

public abstract class Document {

    private ArrayList pages = new ArrayList();

    public Document()
    {
      this.CreerPages();
    }

    public ArrayList getPages()
    {
      return pages;
    }

    protected void addPage(Page p)
    {
      pages.add(p);
    }

    public abstract void CreerPages();
}
