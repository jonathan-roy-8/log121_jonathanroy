/*
   auteur: Sébastien Adam

   source: http://www.dofactory.com/Patterns/Patterns.aspx

*/

public class Curriculum extends Document {
    public void CreerPages()
    {
      addPage(new IdentitePage());
      addPage(new EducationPage());
      addPage(new ExperiencePage());
    }
}
