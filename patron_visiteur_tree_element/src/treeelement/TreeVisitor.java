package treeelement;

public interface TreeVisitor {
	public void visit(Folder f);
	public void visit(File f);
	public void visit(HiddenFile f);
}
