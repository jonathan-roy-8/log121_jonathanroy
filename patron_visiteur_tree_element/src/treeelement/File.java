package treeelement;

public class File implements TreeElement {
	
	protected String name;

	@Override
	public void accept(TreeVisitor v) {
		v.visit(this);
	}

	public File(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
