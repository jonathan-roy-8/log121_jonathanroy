package treeelement;

public class TreeCounter implements TreeVisitor {

	private int currentFolders = 0;
	private int currentFiles = 0;
	private int currentHiddenFiles = 0;

	@Override
	public void visit(Folder f) {
		currentFolders++;

		if (!f.isEmpty()) {
			int saveFolders = currentFolders;
			int saveFiles = currentFiles;
			int saveHiddenFiles = currentHiddenFiles;

			currentFolders = 0;
			currentFiles = 0;
			currentHiddenFiles = 0;

			for (TreeElement e : f.getChilds()) {
				e.accept(this);
			}

			System.out.println("Folder '" + f.getName() + "' contains "
					+ currentFolders + " folders and " + currentFiles
					+ " files (" + currentHiddenFiles + " hidden).");

			currentFolders = saveFolders;
			currentFiles = saveFiles;
			currentHiddenFiles = saveHiddenFiles;
		}
	}

	@Override
	public void visit(File f) {
		currentFiles++;
	}

	@Override
	public void visit(HiddenFile f) {
		currentFiles++;
		currentHiddenFiles++;
	}

}
