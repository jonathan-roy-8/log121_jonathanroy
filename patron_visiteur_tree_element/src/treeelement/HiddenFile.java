package treeelement;

public class HiddenFile extends File {
	
	@Override
	public void accept(TreeVisitor v) {
		v.visit(this);
	}

	public HiddenFile(String name) {
		super(name);
	}
	
}
