package treeelement;

public class TreeDisplayer implements TreeVisitor {
	
	private int indent = 0;

	@Override
	public void visit(Folder f) {
		for(int i = 0; i < indent; i++) {
			System.out.print("\t");
		}
		System.out.print("> " + f.getName());
		System.out.println();
		
		if(!f.isEmpty()) {
			indent++;
			
			for(TreeElement e: f.getChilds()) {
				e.accept(this);
			}
			
			indent--;
		}
	}

	@Override
	public void visit(File f) {
		for(int i = 0; i < indent; i++) {
			System.out.print("\t");
		}
		System.out.print("- " + f.getName());
		System.out.println();
	}

	@Override
	public void visit(HiddenFile f) {
		for(int i = 0; i < indent; i++) {
			System.out.print("\t");
		}
		System.out.print("- **" + f.getName() + "**");
		System.out.println();
	}

}
