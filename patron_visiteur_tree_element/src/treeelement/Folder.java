package treeelement;

import java.util.ArrayList;
import java.util.List;

public class Folder implements TreeElement {
	
	protected String name;
	protected List<TreeElement> childs;

	@Override
	public void accept(TreeVisitor v) {
		v.visit(this);
	}

	public Folder(String name) {
		super();
		this.childs = new ArrayList<TreeElement>();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TreeElement> getChilds() {
		return childs;
	}

	public void setChilds(List<TreeElement> childs) {
		this.childs = childs;
	}
	
	public void addChild(TreeElement child) {
		this.childs.add(child);
	}
	
	public boolean isEmpty() {
		return childs.isEmpty();
	}

}
