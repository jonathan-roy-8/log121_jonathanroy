package treeelement;

public interface TreeElement {
	public void accept(TreeVisitor v);
}
