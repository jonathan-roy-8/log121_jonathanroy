package treeelement;

public class Demo {
	
	public static Folder buildTest() throws Exception {
		java.io.File f = new java.io.File(".");
		
		if(f.exists()) {
			
			Folder root = new Folder(f.getName());
			
			for(java.io.File c : f.listFiles()) {
				TreeElement child;
				if(c.isDirectory()) {
					child = new Folder(c.getName());
					buildRecursively(c, (Folder)child);
				} else {
					if(c.isHidden()) {
						child = new HiddenFile(c.getName());
					} else {
						child = new File(c.getName());
					}
				}
				
				root.addChild(child);
			}
			
			return root;
		}
		
		throw new Exception("Le répertoire n'existe pas.");
	}

	private static void buildRecursively(java.io.File c, Folder cc) {
		for(java.io.File f : c.listFiles()) {
			TreeElement child;
			if(f.isDirectory()) {
				child = new Folder(f.getName());
				buildRecursively(f, (Folder)child);
			} else {
				child = new File(f.getName());
			}
			
			cc.addChild(child);
		}	
	}

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Folder root = buildTest();
		
		TreeVisitor disp = new TreeDisplayer();
		disp.visit(root);
		
		TreeVisitor count = new TreeCounter();
		count.visit(root);
	}

}
