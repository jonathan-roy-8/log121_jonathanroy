/**
 * LOG121 - Patron Adapteur
 */

import javax.swing.*;

/**
 * Classe pour tester un adapteur d'Icon
 */
public class TestAdapter {
	
	public static void main(String[] args){
		System.out.println("TestAdapter: Main");
		
		System.out.println("Create frame");
		JFrame frame = new JFrame("JFrame");
		
		java.net.URL imageURL = TestAdapter.class.getResource("image/image.png");
		
		
		System.out.println("Create imageIcon");
		Icon imIcon = new ImageIcon(imageURL); //L'icon � adapter
		System.out.println(imIcon.toString());
		
		System.out.println("Create IconAdaptor");
		IconAdapter iconAdapter = new IconAdapter(imIcon); // L'adapteur d'icon
		
		//L'icon peut �tre ajout�, via l'adapteur, en tant qu'un JComposent
		System.out.println("Add iconAdaptor to the frame as a JComponent");
		frame.add(iconAdapter); 

		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		System.out.println("Show frame");
		frame.setVisible(true); 
	}
}
