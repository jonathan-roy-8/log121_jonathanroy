import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JComponent;

/**
 * Cette classe permet d'adapter un Icon � un JComponent
 * @author patrice
 *
 */
public class IconAdapter extends JComponent{

	private Icon icon; 
	
	/**
	 * Constructeur
	 * @param icon l'icon � adapter
	 */
	public IconAdapter(Icon icon){
		System.out.println("Instanciate IconAdaptor");
		this.icon = icon;
	}
	
	@Override
	public void paintComponent(Graphics g){
		System.out.println("painComponent");
		icon.paintIcon(this, g, 0, 0);
	}
	
	@Override
	public Dimension getPreferredSize(){
		System.out.println("getPreferedSize");
		return new Dimension( icon.getIconWidth(), icon.getIconHeight() );
	}
}
